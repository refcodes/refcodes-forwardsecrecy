# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifacts provides you means to produce high throughput of symmetric encryption of data securing it with an asymmetric encryption approach - giving you the speed of symmetric encryption and the security of asymmetric (public/private-key) encryption. It seamlessly works together with the [`refcodes-logger`](https://www.metacodes.pro/refcodes/refcodes-logger) toolkit enabling you to log vast amounts of information in an effective and protected manner.***

## Getting started ##

> Please refer to the [refcodes-forwardsecrecy: High encryption throughput](https://www.metacodes.pro/refcodes/refcodes-forwardsecrecy) documentation for an up-to-date and detailed description on the usage of this artifact.  

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-forwardsecrecy</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-forwardsecrecy). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-forwardsecrecy).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-forwardsecrecy/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.

## vt-crypt2 ##

As of [`CRYPT`](https://github.com/dfish3r/vt-crypt2) (see [`vt-crypt2`](https://github.com/dfish3r/vt-crypt2)) the source code below the package `edu.vt.middleware.crypt` is dual licensed under both the LGPL and Apache 2 license. As [`REFCODES.ORG`](http://www.refcodes.org/refcodes) source codes are also licensed under the Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0"), the according Apache 2 license principles are to be applied:

"*... The Apache License is permissive; unlike copyleft licenses, it does not require a derivative work of the software, or modifications to the original, to be distributed using the same license. It still requires application of the same license to all unmodified parts. In every licensed file, original copyright, patent, trademark, and attribution notices must be preserved (excluding notices that do not pertain to any part of the derivative works.) In every licensed file changed, a notification must be added stating that changes have been made to that file...*" ([https://en.wikipedia.org/wiki/Apache_License])

* "*Software can be freely used, modified and distributed in any environment under this license.*"
* "*A copy of the license must be included in the package.*" (&rarr; see `refcodes-licensing` dependency)
* "*Changes to the source code of the software under the Apache license do not have to be sent back to the licensor.*"
* "*Own software that uses software under the Apache license does not have to be under the Apache license.*"
* "*Your own software may only be called Apache if the Apache Foundation has given written permission.*"

([https://de.wikipedia.org/wiki/Apache_License])