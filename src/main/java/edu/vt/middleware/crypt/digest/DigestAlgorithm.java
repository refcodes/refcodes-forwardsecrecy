/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: DigestAlgorithm.java 2744 2013-06-25 20:20:29Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.digest;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.bouncycastle.crypto.Digest;

import edu.vt.middleware.crypt.AbstractRandomizableAlgorithm;
import edu.vt.middleware.crypt.util.Converter;

/**
 * <code>DigestAlgorithm</code> provides aMessage digest operations.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public class DigestAlgorithm extends AbstractRandomizableAlgorithm implements Cloneable {

	/** Chunk size used in stream-based digestion. */
	public static final int CHUNK_SIZE = 4096;

	/** Map of digest algorithm names to classes. */
	private static final Map<String, Class<? extends DigestAlgorithm>> NAME_CLASS_MAP = new HashMap<>();

	/**
	 * Class initializer.
	 */
	static {
		NAME_CLASS_MAP.put( "MD2", MD2.class );
		NAME_CLASS_MAP.put( "MD4", MD4.class );
		NAME_CLASS_MAP.put( "MD5", MD5.class );
		NAME_CLASS_MAP.put( "RIPEMD128", RipeMD128.class );
		NAME_CLASS_MAP.put( "RIPEMD160", RipeMD160.class );
		NAME_CLASS_MAP.put( "RIPEMD256", RipeMD256.class );
		NAME_CLASS_MAP.put( "RIPEMD320", RipeMD320.class );
		NAME_CLASS_MAP.put( "SHA1", SHA1.class );
		NAME_CLASS_MAP.put( "SHA-1", SHA1.class );
		NAME_CLASS_MAP.put( "SHA256", SHA256.class );
		NAME_CLASS_MAP.put( "SHA-256", SHA256.class );
		NAME_CLASS_MAP.put( "SHA384", SHA384.class );
		NAME_CLASS_MAP.put( "SHA-384", SHA384.class );
		NAME_CLASS_MAP.put( "SHA512", SHA512.class );
		NAME_CLASS_MAP.put( "SHA-512", SHA512.class );
		NAME_CLASS_MAP.put( "TIGER", Tiger.class );
		NAME_CLASS_MAP.put( "WHIRLPOOL", Whirlpool.class );
	}

	/** Digest instance used for digest computation. */
	protected Digest digest;

	/** Random data used to initialize digest. */
	protected byte[] salt;

	/**
	 * Creates a new digest algorithm that uses the given {@link Digest} for
	 * digest computation.
	 *
	 * @param d Used for digest computation.
	 */
	protected DigestAlgorithm( final Digest d ) {
		setDigest( d );
	}

	/**
	 * Creates a new digest algorithm instance from its name, e.g. SHA-1 for a
	 * SHA1 digest.
	 *
	 * @param algorithm Name of a aMessage digest algorithm.
	 *
	 * @return Digest algorithm instance that provides the requested algorithm.
	 */
	public static DigestAlgorithm newInstance( final String algorithm ) {
		final Class<? extends DigestAlgorithm> clazz = NAME_CLASS_MAP.get( algorithm.toUpperCase() );
		if ( clazz == null ) {
			throw new IllegalArgumentException( "Digest " + algorithm + " is not available." );
		}
		try {
			// return clazz.newInstance();
			return clazz.getDeclaredConstructor().newInstance();
		}
		catch ( Exception ex ) {
			throw new IllegalArgumentException( ex.getMessage() );
		}
	}

	/**
	 * Sets the internal object responsible for digest computation.
	 *
	 * @param d Used for digest computation.
	 */
	protected void setDigest( final Digest d ) {
		this.digest = d;
		this.algorithm = d.getAlgorithmName();
	}

	/**
	 * Sets the salt used to randomize the digest prior to digestion.
	 *
	 * @param randomBytes Random data to initialize digest.
	 */
	public void setSalt( final byte[] randomBytes ) {
		this.salt = randomBytes;
	}

	/**
	 * Gets a random salt in the amount specified by
	 * {@link #getRandomByteSize()}.
	 *
	 * @return Random salt.
	 */
	public byte[] getRandomSalt() {
		return getRandomData( getRandomByteSize() );
	}

	/**
	 * Gets the underlying object that performs digest computation.
	 *
	 * @return Digest instance.
	 */
	public Digest getDigest() {
		return digest;
	}

	/**
	 * Computes the digest for the given data in a single operation.
	 *
	 * @param input Data to be digested.
	 *
	 * @return Message digest as byte array.
	 */
	public byte[] digest( final byte[] input ) {
		if ( salt != null ) {
			digest.update( salt, 0, salt.length );
		}
		digest.update( input, 0, input.length );

		final byte[] hash = new byte[digest.getDigestSize()];
		digest.doFinal( hash, 0 );
		return hash;
	}

	/**
	 * Computes the digest for the given data in a single operation and passes
	 * the resulting digest bytes through the given converter to produce text
	 * output.
	 *
	 * @param input Data to be digested.
	 * @param converter Converts digest bytes to some string representation, e.g
	 *        Base-64 encoded text.
	 *
	 * @return String representation of digest as provided by the converter.
	 */
	public String digest( final byte[] input, final Converter converter ) {
		return converter.fromBytes( digest( input ) );
	}

	/**
	 * Computes the digest for all the data in the stream by reading data and
	 * hashing it in chunks.
	 *
	 * @param in Input stream containing data to be digested.
	 *
	 * @return Message digest as byte array.
	 *
	 * @throws IOException On input stream read errors.
	 */
	public byte[] digest( final InputStream in ) throws IOException {
		if ( in == null ) {
			throw new IllegalArgumentException( "Input stream cannot be null." );
		}

		final byte[] buffer = new byte[CHUNK_SIZE];
		int count;
		if ( salt != null ) {
			digest.update( salt, 0, salt.length );
		}
		while ( ( count = in.read( buffer, 0, CHUNK_SIZE ) ) > 0 ) {
			digest.update( buffer, 0, count );
		}

		final byte[] hash = new byte[digest.getDigestSize()];
		digest.doFinal( hash, 0 );
		return hash;
	}

	/**
	 * Computes the digest for all the data in the stream by reading data and
	 * hashing it in chunks. The output is converted to a string representation
	 * by the given converter.
	 *
	 * @param in Input stream containing data to be digested.
	 * @param converter Converts digest bytes to some string representation, e.g
	 *        Base-64 encoded text.
	 *
	 * @return String representation of digest as provided by the converter.
	 *
	 * @throws IOException On input stream read errors.
	 */
	public String digest( final InputStream in, final Converter converter ) throws IOException {
		return converter.fromBytes( digest( in ) );
	}

	/** {@inheritDoc} */
	@Override
	public Object clone() throws CloneNotSupportedException {
		final DigestAlgorithm clone = DigestAlgorithm.newInstance( getAlgorithm() );
		clone.setSalt( salt );
		clone.setRandomByteSize( randomByteSize );
		clone.setRandomProvider( randomProvider );
		return clone;
	}
}
