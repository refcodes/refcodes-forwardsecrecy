/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: HexConverter.java 2745 2013-06-25 21:16:10Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2745
 * $ Updated: $Date: 2013-06-25 17:16:10 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;

import org.bouncycastle.util.encoders.Encoder;
import org.bouncycastle.util.encoders.HexEncoder;

/**
 * Converts bytes to HEX and vice versa.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2745 $
 */
public class HexConverter extends AbstractEncodingConverter {

	/** Default byte delimiter. */
	public static final String DEFAULT_BYTE_DELIMITER = ":";

	/** Does encoding work. */
	private final HexEncoder encoder = new HexEncoder();

	/** Flag that determines whether bytes are delimited in string output. */
	private boolean delimitBytesFlag;

	/** Byte delimiter. */
	private String byteDelimiter;

	/** Pattern used to split delimited hex string. */
	private Pattern splitPattern;

	/** Creates a new instance. */
	public HexConverter() {
		this( false );
	}

	/**
	 * Creates a new instance that optionally handled delimited bytes in the
	 * string input/output.
	 *
	 * @param delimitBytes True to handle delimited input and produce delimited
	 *        output strings with delimiter character, false otherwise. If
	 *        enabled, the input/output hexadecimal strings would resemble
	 *        <code>1A:2B:3C:4D</code>. Call {@link #setByteDelimiter(String)}
	 *        to use a delimiter other than {@link #DEFAULT_BYTE_DELIMITER}.
	 *        <p>
	 *        Note: Setting delimited output has the side effect of producing
	 *        uppercase hex characters. This is because several cryptographic
	 *        utilities produce delimited fingerprints with uppercase hex
	 *        characters, so delimited output keeps with that convention since
	 *        it is anticipated to be the common use case for this feature.
	 *        </p>
	 */
	public HexConverter( final boolean delimitBytes ) {
		this.delimitBytesFlag = delimitBytes;
		setByteDelimiter( DEFAULT_BYTE_DELIMITER );
	}

	/**
	 * Gets the byte delmiter string.
	 *
	 * @return Byte delimiter string.
	 */
	public String getByteDelimiter() {
		return byteDelimiter;
	}

	/**
	 * Sets the byte delimiter string. For example, if the delimiter is ":",
	 * then output would resemble <code>1A:2B:3C:4D</code>.
	 *
	 * @param delim Byte delimiter string.
	 */
	public void setByteDelimiter( final String delim ) {
		this.byteDelimiter = delim;
		if ( this.delimitBytesFlag ) {
			splitPattern = Pattern.compile( byteDelimiter );
		}
	}

	/** {@inheritDoc} */
	@Override
	public String fromBytes( final byte[] input, final int offset, final int length ) {
		if ( delimitBytesFlag ) {
			final byte[] delimBytes = byteDelimiter.getBytes();
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			for ( int i = offset; i < length; i++ ) {
				try {
					if ( i > offset ) {
						out.write( delimBytes );
					}
					encoder.encode( input, offset + i, 1, out );
				}
				catch ( IOException e ) {
					throw new IllegalArgumentException( e.getMessage() );
				}
			}
			// Use the default character set since the delimiter likely comes
			// from platform character set, which could possibly be outside ASCII
			return out.toString().toUpperCase();
		}
		else {
			return super.fromBytes( input, offset, length );
		}
	}

	/** {@inheritDoc} */
	@Override
	public byte[] toBytes( final String input ) {
		if ( delimitBytesFlag ) {
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			final String[] hexBytes = splitPattern.split( input );
			for ( String hexByte : hexBytes ) {
				try {
					encoder.decode( hexByte, out );
				}
				catch ( IOException e ) {
					throw new IllegalArgumentException( e.getMessage() );
				}
			}
			return out.toByteArray();
		}
		else {
			return super.toBytes( input );
		}
	}

	/** {@inheritDoc} */
	@Override
	protected Encoder getEncoder() {
		return encoder;
	}
}
