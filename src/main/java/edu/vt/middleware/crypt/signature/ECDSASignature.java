/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: ECDSASignature.java 2744 2013-06-25 20:20:29Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.signature;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;

import edu.vt.middleware.crypt.digest.DigestAlgorithm;
import edu.vt.middleware.crypt.digest.SHA1;

/**
 * Implements the ECDSA algorithm.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public class ECDSASignature extends AbstractDSASignature {

	/** Signature algorithm name. */
	private static final String ALGORITHM = "ECDSA";

	/**
	 * Creates a new ECDSA signature instance that uses SHA-1 for computation of
	 * aMessage digests.
	 */
	public ECDSASignature() {
		this( new SHA1() );
	}

	/**
	 * Creates a new ECDSA signature instance that uses the given digest
	 * algorithm for aMessage digest computation.
	 *
	 * @param d Message digest algorithm.
	 */
	public ECDSASignature( final DigestAlgorithm d ) {
		super( ALGORITHM );
		digest = d;
		signer = new ECDSASigner();
	}

	/** {@inheritDoc} */
	@Override
	public void setSignKey( final PrivateKey key ) {
		if ( !ECPrivateKey.class.isInstance( key ) ) {
			throw new IllegalArgumentException( "EC private key required." );
		}
		super.setSignKey( key );
	}

	/** {@inheritDoc} */
	@Override
	public void setVerifyKey( final PublicKey key ) {
		if ( !ECPublicKey.class.isInstance( key ) ) {
			throw new IllegalArgumentException( "EC public key required." );
		}
		super.setVerifyKey( key );
	}

	/** {@inheritDoc} */
	@Override
	public void initSign() {
		if ( signKey == null ) {
			throw new IllegalStateException( "Sign key must be set prior to initialization." );
		}
		try {
			init( true, ECUtil.generatePrivateKeyParameter( signKey ) );
		}
		catch ( InvalidKeyException e ) {
			throw new RuntimeException( "Cannot convert private key to BC format", e );
		}
	}

	/** {@inheritDoc} */
	@Override
	public void initVerify() {
		if ( verifyKey == null ) {
			throw new IllegalStateException( "Verify key must be set prior to initialization." );
		}
		try {
			init( false, ECUtil.generatePublicKeyParameter( verifyKey ) );
		}
		catch ( InvalidKeyException e ) {
			throw new RuntimeException( "Cannot convert public key to BC format", e );
		}
	}

	/**
	 * Initialize the signer.
	 *
	 * @param forSigning Whether to initialize signer for the sign operation.
	 * @param params BC cipher parameters.
	 */
	@Override
	protected void init( final boolean forSigning, final CipherParameters params ) {
		if ( forSigning && randomProvider != null ) {
			signer.init( forSigning, new ParametersWithRandom( params, randomProvider ) );
		}
		else {
			signer.init( forSigning, params );
		}
	}
}
