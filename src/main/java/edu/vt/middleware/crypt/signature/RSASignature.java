/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: RSASignature.java 2745 2013-06-25 21:16:10Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2745
 * $ Updated: $Date: 2013-06-25 17:16:10 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.signature;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.signers.RSADigestSigner;

import edu.vt.middleware.crypt.CryptException;
import edu.vt.middleware.crypt.digest.DigestAlgorithm;
import edu.vt.middleware.crypt.digest.SHA1;

/**
 * Implements the RSASSA-PKCS1-v1_5 signature algorithm described in
 * <a href="http://www.ietf.org/rfc/rfc2437.txt">
 * http://www.ietf.org/rfc/rfc2437.txt</a>.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2745 $
 */
public class RSASignature extends SignatureAlgorithm {

	/** Signature algorithm name. */
	private static final String ALGORITHM = "RSA";

	/** Implements the RSA signature operation. */
	private final RSADigestSigner signer;

	/**
	 * Creates a new RSA signature class that uses a SHA-1 for aMessage digest
	 * computation.
	 */
	public RSASignature() {
		this( new SHA1() );
	}

	/**
	 * Creates a new RSA signature class that uses the given digest algorithm
	 * for aMessage digest computation.
	 *
	 * @param d Message digest algorithm.
	 */
	public RSASignature( final DigestAlgorithm d ) {
		super( ALGORITHM );
		digest = d;
		signer = new RSADigestSigner( d.getDigest() );
	}

	/** {@inheritDoc} */
	@Override
	public void setSignKey( final PrivateKey key ) {
		if ( !RSAPrivateKey.class.isInstance( key ) ) {
			throw new IllegalArgumentException( "RSA private key required." );
		}
		super.setSignKey( key );
	}

	/** {@inheritDoc} */
	@Override
	public void setVerifyKey( final PublicKey key ) {
		if ( !RSAPublicKey.class.isInstance( key ) ) {
			throw new IllegalArgumentException( "RSA public key required." );
		}
		super.setVerifyKey( key );
	}

	/** {@inheritDoc} */
	@Override
	public void initSign() {
		if ( signKey == null ) {
			throw new IllegalStateException( "Sign key must be set prior to initialization." );
		}

		final RSAPrivateKey privKey = (RSAPrivateKey) signKey;
		init( true, new RSAKeyParameters( true, privKey.getModulus(), privKey.getPrivateExponent() ) );
	}

	/** {@inheritDoc} */
	@Override
	public void initVerify() {
		if ( verifyKey == null ) {
			throw new IllegalStateException( "Verify key must be set prior to initialization." );
		}

		final RSAPublicKey pubKey = (RSAPublicKey) verifyKey;
		init( false, new RSAKeyParameters( false, pubKey.getModulus(), pubKey.getPublicExponent() ) );
	}

	/** {@inheritDoc} */
	@Override
	public byte[] sign( final byte[] data ) throws CryptException {
		signer.update( data, 0, data.length );
		try {
			return signer.generateSignature();
		}
		catch ( DataLengthException e ) {
			throw new CryptException( "Data is too long for aMessage digest.", e );
		}
		catch ( CryptoException e ) {
			throw new CryptException( "Cryptographic error.", e );
		}
	}

	/** {@inheritDoc} */
	@Override
	public byte[] sign( final InputStream in ) throws CryptException, IOException {
		chunkUpdate( in );
		try {
			return signer.generateSignature();
		}
		catch ( DataLengthException e ) {
			throw new CryptException( "Data is too long for aMessage digest.", e );
		}
		catch ( CryptoException e ) {
			throw new CryptException( "Cryptographic error.", e );
		}
	}

	/** {@inheritDoc} */
	@Override
	public boolean verify( final byte[] data, final byte[] signature ) throws CryptException {
		signer.update( data, 0, data.length );
		return signer.verifySignature( signature );
	}

	/** {@inheritDoc} */
	@Override
	public boolean verify( final InputStream in, final byte[] signature ) throws CryptException, IOException {
		chunkUpdate( in );
		return signer.verifySignature( signature );
	}

	/**
	 * Initialize the signer.
	 *
	 * @param forSigning Whether to initialize signer for the sign operation.
	 * @param params BC cipher parameters.
	 */
	protected void init( final boolean forSigning, final CipherParameters params ) {
		if ( randomProvider != null ) {
			signer.init( forSigning, new ParametersWithRandom( params, randomProvider ) );
		}
		else {
			signer.init( forSigning, params );
		}
	}

	/**
	 * Update the signer in chunks with data read from the input stream.
	 *
	 * @param in Input stream to read from.
	 *
	 * @throws IOException On IO errors.
	 */
	protected void chunkUpdate( final InputStream in ) throws IOException {
		final byte[] buffer = new byte[DigestAlgorithm.CHUNK_SIZE];
		int count;
		while ( ( count = in.read( buffer, 0, DigestAlgorithm.CHUNK_SIZE ) ) > 0 ) {
			signer.update( buffer, 0, count );
		}
	}
}
