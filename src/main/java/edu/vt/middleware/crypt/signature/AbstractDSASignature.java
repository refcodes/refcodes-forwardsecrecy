/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: AbstractDSASignature.java 2744 2013-06-25 20:20:29Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.signature;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1OutputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DSA;
import org.bouncycastle.crypto.params.ParametersWithRandom;

import edu.vt.middleware.crypt.CryptException;

/**
 * Base class for all signatures that implement the DSA scheme.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public abstract class AbstractDSASignature extends SignatureAlgorithm {

	/** Signer that implements DSA algorithm. */
	protected DSA signer;

	/**
	 * Creates a new instance of a the given signature algorithm.
	 *
	 * @param alg Signature algorithm name, e.g. DSA, RSA.
	 */
	protected AbstractDSASignature( final String alg ) {
		super( alg );
	}

	/** {@inheritDoc} */
	@Override
	public byte[] sign( final byte[] data ) throws CryptException {
		final BigInteger[] out = signer.generateSignature( digest.digest( data ) );
		return encode( out[0], out[1] );
	}

	/** {@inheritDoc} */
	@Override
	public byte[] sign( final InputStream in ) throws CryptException, IOException {
		final BigInteger[] out = signer.generateSignature( digest.digest( in ) );
		return encode( out[0], out[1] );
	}

	/** {@inheritDoc} */
	@Override
	public boolean verify( final byte[] data, final byte[] signature ) throws CryptException {
		final BigInteger[] sig = decode( signature );
		return signer.verifySignature( digest.digest( data ), sig[0], sig[1] );
	}

	/** {@inheritDoc} */
	@Override
	public boolean verify( final InputStream in, final byte[] signature ) throws CryptException, IOException {
		final BigInteger[] sig = decode( signature );
		return signer.verifySignature( digest.digest( in ), sig[0], sig[1] );
	}

	/**
	 * Initialize the signer.
	 *
	 * @param forSigning Whether to initialize signer for the sign operation.
	 * @param params BC cipher parameters.
	 */
	protected void init( final boolean forSigning, final CipherParameters params ) {
		if ( forSigning && randomProvider != null ) {
			signer.init( forSigning, new ParametersWithRandom( params, randomProvider ) );
		}
		else {
			signer.init( forSigning, params );
		}
	}

	/**
	 * Produces DER-encoded byte array output from the raw DSA signature output
	 * parameters, r and s.
	 *
	 * @param r DSA signature output integer r.
	 * @param s DSA signature output integer s.
	 *
	 * @return DER-encoded concatenation of byte representations of r and s.
	 *
	 * @throws edu.vt.middleware.crypt.CryptException On cryptographic errors.
	 */
	private byte[] encode( final BigInteger r, final BigInteger s ) throws CryptException {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final ASN1EncodableVector v = new ASN1EncodableVector();
		v.add( new ASN1Integer( r ) );
		v.add( new ASN1Integer( s ) );
		try {
			ASN1OutputStream.create( out ).writeObject( new DERSequence( v ) );
		}
		catch ( IOException e ) {
			throw new CryptException( "Error encoding DSA signature.", e );
		}
		return out.toByteArray();
	}

	/**
	 * Produces the r,s integer pair of a DSA signature from a DER-encoded byte
	 * representation.
	 *
	 * @param in DER-encoded concatenation of byte representation of r and s.
	 *
	 * @return DSA signature output parameters (r,s).
	 *
	 * @throws edu.vt.middleware.crypt.CryptException On cryptographic errors.
	 */
	private BigInteger[] decode( final byte[] in ) throws CryptException {
		final ASN1Sequence s;
		try ( ASN1InputStream theAsn1InputStream = new ASN1InputStream( in ) ) {
			s = (ASN1Sequence) theAsn1InputStream.readObject();
			return new BigInteger[] { ( (ASN1Integer) s.getObjectAt( 0 ) ).getValue(), ( (ASN1Integer) s.getObjectAt( 1 ) ).getValue(), };
		}
		catch ( IOException e ) {
			throw new CryptException( "Error decoding DSA signature.", e );
		}
	}
}
