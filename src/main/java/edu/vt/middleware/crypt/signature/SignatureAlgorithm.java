/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: SignatureAlgorithm.java 2744 2013-06-25 20:20:29Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.signature;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

import edu.vt.middleware.crypt.AbstractAlgorithm;
import edu.vt.middleware.crypt.CryptException;
import edu.vt.middleware.crypt.digest.DigestAlgorithm;
import edu.vt.middleware.crypt.util.Converter;

/**
 * <code>SignatureAlgorithm</code> provides aMessage signing and verification
 * operations.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public class SignatureAlgorithm extends AbstractAlgorithm {

	/** Map of signature algorithm names to classes. */
	private static final Map<String, Class<? extends SignatureAlgorithm>> NAME_CLASS_MAP = new HashMap<>();

	/**
	 * Class initializer.
	 */
	static {
		NAME_CLASS_MAP.put( "DSA", DSASignature.class );
		NAME_CLASS_MAP.put( "ECDSA", ECDSASignature.class );
		NAME_CLASS_MAP.put( "RSA", RSASignature.class );
	}

	/** Private key used for signing. */
	protected PrivateKey signKey;

	/** Public key used for verification. */
	protected PublicKey verifyKey;

	/** Message digest used to compute encoded aMessage for signing. */
	protected DigestAlgorithm digest;

	/**
	 * Creates a new instance of a the given signature algorithm.
	 *
	 * @param alg Signature algorithm name, e.g. DSA, RSA.
	 */
	protected SignatureAlgorithm( final String alg ) {
		this.algorithm = alg;
	}

	/**
	 * Creates a new instance that uses the given signature algorithm to compute
	 * and verify signatures.
	 *
	 * @param algorithm Signature algorithm name.
	 *
	 * @return New instance of signature algorithm.
	 */
	public static SignatureAlgorithm newInstance( final String algorithm ) {
		final Class<? extends SignatureAlgorithm> clazz = NAME_CLASS_MAP.get( algorithm.toUpperCase() );
		if ( clazz == null ) {
			throw new IllegalArgumentException( "Signature " + algorithm + " is not available." );
		}
		try {
			// return clazz.newInstance();
			return clazz.getDeclaredConstructor().newInstance();
		}
		catch ( Exception ex ) {
			throw new IllegalArgumentException( ex.getMessage() );
		}
	}

	/**
	 * Creates a new instance that uses the given signature algorithm and digest
	 * algorithm to compute and verify signatures.
	 *
	 * @param algorithm Signature algorithm name.
	 * @param digestAlgorithm Message digest algorithm name.
	 *
	 * @return New instance of signature algorithm.
	 */
	public static SignatureAlgorithm newInstance( final String algorithm, final String digestAlgorithm ) {
		final Class<? extends SignatureAlgorithm> clazz = NAME_CLASS_MAP.get( algorithm.toUpperCase() );
		if ( clazz == null ) {
			throw new IllegalArgumentException( "Signature " + algorithm + " is not available." );
		}
		try {
			final Constructor<? extends SignatureAlgorithm> cons = clazz.getConstructor( new Class[] { DigestAlgorithm.class } );
			return cons.newInstance( new Object[] { DigestAlgorithm.newInstance( digestAlgorithm ) } );
		}
		catch ( Exception ex ) {
			throw new IllegalArgumentException( ex.getMessage() );
		}
	}

	/**
	 * Sets the private key used for signing.
	 *
	 * @param key Private key.
	 */
	public void setSignKey( final PrivateKey key ) {
		this.signKey = key;
	}

	/**
	 * Sets the public key used for verification.
	 *
	 * @param key Public key.
	 */
	public void setVerifyKey( final PublicKey key ) {
		this.verifyKey = key;
	}

	/**
	 * Initialize the signature for the {@link #sign(byte[])} operation.
	 *
	 * @throws CryptException On signature initialization failure.
	 */
	public void initSign() throws CryptException {
		throw new UnsupportedOperationException( "Not implemented." );
	}

	/**
	 * Initialize the signature for the {@link #verify(byte[], byte[])}
	 * operation.
	 *
	 * @throws CryptException On verification initialization failure.
	 */
	public void initVerify() throws CryptException {
		throw new UnsupportedOperationException( "Not implemented." );
	}

	/**
	 * Signs the given data and returns the signature as a byte array.
	 *
	 * @param data Data to be signed.
	 *
	 * @return Signature of given data as byte array.
	 *
	 * @throws CryptException On signature failure.
	 */
	public byte[] sign( final byte[] data ) throws CryptException {
		throw new UnsupportedOperationException( "Not implemented." );
	}

	/**
	 * Signs the given data and returns the signature as a string using the
	 * conversion strategy provided by the given converter.
	 *
	 * @param data Data to be signed.
	 * @param converter Converts raw signature bytes to a string.
	 *
	 * @return Signature of given data as a string.
	 *
	 * @throws CryptException On signature failure.
	 */
	public String sign( final byte[] data, final Converter converter ) throws CryptException {
		return converter.fromBytes( sign( data ) );
	}

	/**
	 * Computes the signature of the data in the given input stream by
	 * processing in chunks.
	 *
	 * @param in Input stream containing data to be signed.
	 *
	 * @return Signature of given data as byte array.
	 *
	 * @throws CryptException On signature failure.
	 * @throws IOException On input stream read errors.
	 */
	public byte[] sign( final InputStream in ) throws CryptException, IOException {
		throw new UnsupportedOperationException( "Not implemented." );
	}

	/**
	 * Computes the signature of the data in the given input stream by
	 * processing in chunks.
	 *
	 * @param in Input stream containing data to be signed.
	 * @param converter Converts raw signature bytes to a string.
	 *
	 * @return Signature of given data as a string.
	 *
	 * @throws CryptException On signature failure.
	 * @throws IOException On input stream read errors.
	 */
	public String sign( final InputStream in, final Converter converter ) throws CryptException, IOException {
		return converter.fromBytes( sign( in ) );
	}

	/**
	 * Verifies the signature of the given data matches the given signature.
	 *
	 * @param data Data to be verified.
	 * @param signature Signature to be used for comparison.
	 *
	 * @return True if the signed data matches the given signature, false
	 *         otherwise.
	 *
	 * @throws CryptException On verification failure.
	 */
	public boolean verify( final byte[] data, final byte[] signature ) throws CryptException {
		throw new UnsupportedOperationException( "Not implemented." );
	}

	/**
	 * Verifies the signature of the given data matches the given signature.
	 *
	 * @param data Data to be verified.
	 * @param signature String representation of signature to be used for
	 *        comparison.
	 * @param converter Converts the signature string representation into raw
	 *        bytes required for verification.
	 *
	 * @return True if the signed data matches the given signature, false
	 *         otherwise.
	 *
	 * @throws CryptException On verification failure.
	 */
	public boolean verify( final byte[] data, final String signature, final Converter converter ) throws CryptException {
		return verify( data, converter.toBytes( signature ) );
	}

	/**
	 * Verifies the signature of the given data matches the given signature.
	 *
	 * @param in Input stream containing data to be verified.
	 * @param signature Signature to be used for comparison.
	 *
	 * @return True if the signed data matches the given signature, false
	 *         otherwise.
	 *
	 * @throws CryptException On verification failure.
	 * @throws IOException On input stream read errors.
	 */
	public boolean verify( final InputStream in, final byte[] signature ) throws CryptException, IOException {
		throw new UnsupportedOperationException( "Not implemented." );
	}

	/**
	 * Verifies the signature of the data in the given input stream matches the
	 * given signature.
	 *
	 * @param in Input stream containing data to be verified.
	 * @param signature String representation of signature to be used for
	 *        comparison.
	 * @param converter Converts the signature string representation into raw
	 *        bytes required for verification.
	 *
	 * @return True if the signed data matches the given signature, false
	 *         otherwise.
	 *
	 * @throws CryptException On verification failure.
	 * @throws IOException On input stream read errors.
	 */
	public boolean verify( final InputStream in, final String signature, final Converter converter ) throws CryptException, IOException {
		return verify( in, converter.toBytes( signature ) );
	}

	/** {@inheritDoc} */
	@Override
	public Object clone() throws CloneNotSupportedException {
		final SignatureAlgorithm clone = SignatureAlgorithm.newInstance( getAlgorithm(), digest.getAlgorithm() );
		clone.setRandomProvider( randomProvider );
		clone.setSignKey( signKey );
		clone.setVerifyKey( verifyKey );
		return clone;
	}
}
