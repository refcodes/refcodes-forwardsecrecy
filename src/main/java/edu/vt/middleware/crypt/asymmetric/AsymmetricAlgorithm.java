/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: AsymmetricAlgorithm.java 2744 2013-06-25 20:20:29Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.asymmetric;

import java.security.spec.AlgorithmParameterSpec;
import java.util.HashMap;
import java.util.Map;

import edu.vt.middleware.crypt.AbstractEncryptionAlgorithm;

/**
 * Provides asymmetric encryption and decryption operations using a
 * public/private key pair.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public class AsymmetricAlgorithm extends AbstractEncryptionAlgorithm {

	/** Mode used for encryption and decryption. */
	public static final String MODE = "NONE";

	/** Padding used for encryption and decryption. */
	public static final String PADDING = "OAEPPadding";

	/** Size of chunks in stream-based encryption. */
	private static final int CHUNK_SIZE = 2048;

	/** Map of digest algorithm names to classes. */
	private static final Map<String, Class<? extends AsymmetricAlgorithm>> NAME_CLASS_MAP = new HashMap<>();

	/**
	 * Class initializer.
	 */
	static {
		NAME_CLASS_MAP.put( "RSA", RSA.class );
	}

	/**
	 * Creates a new instance that uses a cipher of the given algorithm and the
	 * default mode and padding.
	 *
	 * @param cipherAlgorithm Cipher algorithm name.
	 */
	protected AsymmetricAlgorithm( final String cipherAlgorithm ) {
		super( cipherAlgorithm, MODE, PADDING );
	}

	/**
	 * Creates a new instance that uses a cipher of the given name.
	 *
	 * @param algorithm Cipher algorithm name.
	 *
	 * @return Asymmetric algorithm instance that implements the given cipher
	 *         algorithm.
	 */
	public static AsymmetricAlgorithm newInstance( final String algorithm ) {
		final Class<? extends AsymmetricAlgorithm> clazz = NAME_CLASS_MAP.get( algorithm.toUpperCase() );
		if ( clazz != null ) {
			try {
				// return clazz.newInstance();
				return clazz.getDeclaredConstructor().newInstance();
			}
			catch ( Exception ex ) {
				throw new IllegalArgumentException( ex.getMessage() );
			}
		}
		else {
			// Search provider
			return new AsymmetricAlgorithm( algorithm );
		}
	}

	/** {@inheritDoc} */
	@Override
	public Object clone() throws CloneNotSupportedException {
		final AsymmetricAlgorithm clone = AsymmetricAlgorithm.newInstance( getAlgorithm() );
		clone.setRandomProvider( randomProvider );
		clone.setKey( key );
		return clone;
	}

	/** {@inheritDoc} */
	@Override
	protected AlgorithmParameterSpec getAlgorithmParameterSpec() {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	protected int getChunkSize() {
		return CHUNK_SIZE;
	}
}
