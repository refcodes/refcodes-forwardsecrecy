/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: AbstractAlgorithm.java 2745 2013-06-25 21:16:10Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2745
 * $ Updated: $Date: 2013-06-25 17:16:10 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt;

import java.security.SecureRandom;

/**
 * Abstract cryptographic algorithm that is the basis of digest, encryption, and
 * signature algorithms.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2745 $
 */
public abstract class AbstractAlgorithm implements Algorithm {

	/** Default number of random bytes. */
	private static final int DEFAULT_RANDOM_BYTE_SIZE = 256;

	/** Logger instance. */
	// protected final Logger logger = LoggerFactory.getLogger( getClass() );

	/** Algorithm name. */
	protected String algorithm;

	/** DatagramsDestination of secure random data. */
	protected SecureRandom randomProvider;

	/** Number of bytes used for random data needs. */
	protected int randomByteSize = DEFAULT_RANDOM_BYTE_SIZE;

	/** {@inheritDoc} */
	@Override
	public String getAlgorithm() {
		return this.algorithm;
	}

	/** {@inheritDoc} */
	@Override
	public void setRandomProvider( final SecureRandom random ) {
		this.randomProvider = random;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return this.algorithm;
	}

	/** {@inheritDoc} */
	@Override
	public byte[] getRandomData( final int nBytes ) {
		if ( this.randomProvider == null ) {
			throw new IllegalStateException( "No random provider available." );
		}

		final byte[] data = new byte[nBytes];
		randomProvider.nextBytes( data );
		return data;
	}
}
