/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: AbstractEncodingFilterInputStream.java 2744 2013-06-25 20:20:29Z dfisher
 * $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Abstract base class for filter input streams that decode encoded character
 * bytes into raw bytes.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public abstract class AbstractEncodingFilterInputStream extends FilterInputStream {

	/** Number of encoded bytes to read in one buffer filling round. */
	protected static final int CHUNK_SIZE = 2048;

	/** Buffer containing decoded bytes. */
	protected final DirectByteArrayOutputStream decodeBuffer;

	/** Holds bytes encoded bytes read from input stream. */
	protected final byte[] byteBuffer = new byte[CHUNK_SIZE];

	/** Position in decoded byte buffer. */
	protected int position;

	/**
	 * Creates an input filter that decodes characters in the given input
	 * stream.
	 *
	 * @param in Input stream to wrap.
	 */
	protected AbstractEncodingFilterInputStream( final InputStream in ) {
		super( in );
		decodeBuffer = new DirectByteArrayOutputStream( getDecodeBufferCapacity() );
	}

	/** {@inheritDoc} */
	@Override
	public int read() throws IOException {
		if ( position == decodeBuffer.size() || decodeBuffer.size() == 0 ) {
			fillBuffer();
		}
		if ( decodeBuffer.size() > 0 ) {
			return decodeBuffer.getBuffer()[position++];
		}
		else {
			return -1;
		}
	}

	/** {@inheritDoc} */
	@Override
	public int read( final byte[] b ) throws IOException {
		return read( b, 0, b.length );
	}

	/** {@inheritDoc} */
	@Override
	public int read( final byte[] b, final int off, final int len ) throws IOException {
		int count = 0;
		while ( count < len ) {
			if ( position == decodeBuffer.size() || decodeBuffer.size() == 0 ) {
				fillBuffer();
			}
			if ( decodeBuffer.size() == 0 ) {
				break;
			}
			b[off + count++] = decodeBuffer.getBuffer()[position++];
		}
		return count;
	}

	/**
	 * Gets the encoder that decodes encoded character data in the input stream
	 * to raw bytes.
	 *
	 * @return Encoder instance.
	 */
	protected abstract int getDecodeBufferCapacity();

	/**
	 * Reads characters from the input reader and decodes them to fill
	 * {@link #decodeBuffer}.
	 *
	 * @throws IOException On read errors.
	 */
	protected abstract void fillBuffer() throws IOException;
}
