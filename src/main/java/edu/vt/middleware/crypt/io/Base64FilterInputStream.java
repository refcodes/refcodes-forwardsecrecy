/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: Base64FilterInputStream.java 2745 2013-06-25 21:16:10Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2745
 * $ Updated: $Date: 2013-06-25 17:16:10 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.io;

import java.io.IOException;
import java.io.InputStream;

import org.bouncycastle.util.encoders.Base64Encoder;

/**
 * Converts an input stream of base-64 encoded character bytes into raw bytes.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2745 $
 */
public class Base64FilterInputStream extends AbstractEncodingFilterInputStream {

	/** Wrap lines at 64 characters. */
	public static final int LINE_LENGTH_64 = 64;

	/** Wrap lines at 76 characters. */
	public static final int LINE_LENGTH_76 = 76;

	/** Number of bytes encoded into a 24-bit base-64 encoded group. */
	private static final int BYTES_PER_GROUP = 3;

	/** Number of characters encoded into a 24-bit base-64 encoded group. */
	private static final int CHARS_PER_GROUP = 4;

	/** Does work of base-64 encoding. */
	private final Base64Encoder encoder = new Base64Encoder();

	/** Length of lines of encoded input. 0 indicates no line wrapping. */
	private int lineLength;

	/**
	 * Creates a base-64 filter input stream around the given input stream.
	 *
	 * @param in Input stream to wrap.
	 */
	public Base64FilterInputStream( final InputStream in ) {
		this( in, 0 );
	}

	/**
	 * Creates a base-64 filter input stream around the given input stream.
	 *
	 * @param in Input stream to wrap.
	 * @param charsPerLine Number of characters per line of encoded input. Must
	 *        be one of {@link #LINE_LENGTH_64}, {@link #LINE_LENGTH_76}, or 0
	 *        to indicate no wrapping.
	 */
	public Base64FilterInputStream( final InputStream in, final int charsPerLine ) {
		super( in );
		if ( charsPerLine != 0 && charsPerLine != LINE_LENGTH_64 && charsPerLine != LINE_LENGTH_76 ) {
			throw new IllegalArgumentException( "Invalid characters per line." );
		}
		lineLength = charsPerLine;
	}

	/** {@inheritDoc} */
	@Override
	protected int getDecodeBufferCapacity() {
		return CHUNK_SIZE * BYTES_PER_GROUP / CHARS_PER_GROUP;
	}

	/** {@inheritDoc} */
	@Override
	protected void fillBuffer() throws IOException {
		position = 0;
		decodeBuffer.reset();

		int count = 0;
		if ( lineLength == 0 ) {
			count = in.read( byteBuffer );
		}
		else {
			int n;
			while ( count + lineLength < CHUNK_SIZE ) {
				n = in.read( byteBuffer, count, lineLength );
				if ( n < 0 ) {
					break;
				}
				count += n;
				if ( count % lineLength == 0 ) {
					// Skip line terminator character
					in.skip( 1 );
				}
			}
		}
		if ( count > 0 ) {
			encoder.decode( byteBuffer, 0, count, decodeBuffer );
		}
	}
}
