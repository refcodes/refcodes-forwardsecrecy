/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: TeePrintStream.java 2745 2013-06-25 21:16:10Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2745
 * $ Updated: $Date: 2013-06-25 17:16:10 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Works like the Unix <code>tee</code> utility by writing to two streams
 * simultaneously, where one is typically STDOUT or STDERR.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2745 $
 */
public class TeePrintStream extends PrintStream {

	/** Other output stream. */
	private final OutputStream other;

	/**
	 * Creates a tee stream that writes to both of the given streams
	 * simultaneously. To operate like the Unix <code>tee</code>, the second
	 * stream should be STDOUT or STDERR.
	 *
	 * @param out1 Primary output stream.
	 * @param out2 Secondary output stream, usually <code>System.out</code> or
	 *        <code>System.err</code>.
	 */
	public TeePrintStream( final OutputStream out1, final OutputStream out2 ) {
		super( out1 );
		other = out2;
	}

	/** {@inheritDoc} */
	@Override
	public void write( final int b ) {
		super.write( b );
		try {
			other.write( b );
		}
		catch ( IOException e ) {
			throw new RuntimeException( "Error writing to secondary stream." );
		}
	}

	/** {@inheritDoc} */
	@Override
	public void write( final byte[] buf, final int off, final int len ) {
		super.write( buf, off, len );
		try {
			other.write( buf, off, len );
		}
		catch ( IOException e ) {
			throw new RuntimeException( "Error writing to secondary stream." );
		}
	}

	/** {@inheritDoc} */
	@Override
	public void flush() {
		super.flush();
		try {
			other.flush();
		}
		catch ( IOException e ) {
			throw new RuntimeException( "Error flushing secondary stream." );
		}
	}
}
