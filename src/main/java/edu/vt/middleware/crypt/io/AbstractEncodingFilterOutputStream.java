/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: AbstractEncodingFilterOutputStream.java 2744 2013-06-25 20:20:29Z
 * dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2744
 * $ Updated: $Date: 2013-06-25 16:20:29 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Abstract base class for filter output streams that convert raw bytes into
 * encoded characters.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2744 $
 */
public abstract class AbstractEncodingFilterOutputStream extends FilterOutputStream {

	/**
	 * Creates an output filter that encodes raw bytes into encoded character
	 * bytes in the wrapped output stream.
	 *
	 * @param out Output stream to wrap.
	 */
	protected AbstractEncodingFilterOutputStream( final OutputStream out ) {
		super( out );
	}

	/** {@inheritDoc} */
	@Override
	public void write( final int b ) throws IOException {
		final byte[] buffer = new byte[1];
		buffer[0] = (byte) b;
		writeEncoded( buffer, 0, 1 );
	}

	/** {@inheritDoc} */
	@Override
	public void write( final byte[] b ) throws IOException {
		writeEncoded( b, 0, b.length );
	}

	/** {@inheritDoc} */
	@Override
	public void write( final byte[] b, final int off, final int len ) throws IOException {
		writeEncoded( b, off, len );
	}

	/**
	 * Encode the given raw bytes and write the result to the wrapped output
	 * stream.
	 *
	 * @param data Raw bytes to be encoded and written.
	 * @param offset Starting index of data to be written in input byte array.
	 * @param length Number of bytes to be written from input byte array.
	 *
	 * @throws IOException On write errors.
	 */
	protected abstract void writeEncoded( final byte[] data, final int offset, final int length ) throws IOException;
}
