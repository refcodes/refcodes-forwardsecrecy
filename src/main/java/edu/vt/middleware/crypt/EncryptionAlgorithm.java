/**
 * As of "https://github.com/dfish3r/vt-crypt2 "CRYPT" (vt-crypt2) the source
 * code below the package "edu.vt.middleware.crypt" is dual licensed under both
 * the LGPL and Apache 2 license. As REFCODES.ORG source codes are also licensed
 * under the Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0"),
 * the according Apache 2 license principles are to be applied: "... The Apache
 * License is permissive; unlike copyleft licenses, it does not require a
 * derivative work of the software, or modifications to the original, to be
 * distributed using the same license. It still requires application of the same
 * license to all unmodified parts. In every licensed file, original copyright,
 * patent, trademark, and attribution notices must be preserved (excluding
 * notices that do not pertain to any part of the derivative works.) In every
 * licensed file changed, a notification must be added stating that changes have
 * been made to that file..." ("https://en.wikipedia.org/wiki/Apache_License")
 * <ul>
 * <li>"Software can be freely used, modified and distributed in any environment
 * under this license."
 * <li>"A copy of the license must be included in the package." (&rarr; see
 * <code>refcodes-licensing</code> dependency)
 * <li>"Changes to the source code of the software under the Apache license do
 * not have to be sent back to the licensor."
 * <li>"Own software that uses software under the Apache license does not have
 * to be under the Apache license."
 * <li>"Your own software may only be called Apache if the Apache Foundation has
 * given written permission."
 * </ul>
 * (freely translated from "https://de.wikipedia.org/wiki/Apache_License")
 */

/*
 * $Id: EncryptionAlgorithm.java 2745 2013-06-25 21:16:10Z dfisher $
 * 
 * Copyright (C) 2003-2013 Virginia Tech. All rights reserved.
 * 
 * SEE TEXT FOR MORE INFORMATION
 * 
 * Author: Middleware Services Email: middleware@vt.edu Version: $Revision: 2745
 * $ Updated: $Date: 2013-06-25 17:16:10 -0400 (Tue, 25 Jun 2013) $
 */
package edu.vt.middleware.crypt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;

import edu.vt.middleware.crypt.io.Base64FilterInputStream;
import edu.vt.middleware.crypt.io.HexFilterInputStream;
import edu.vt.middleware.crypt.util.Converter;

/**
 * Describes operations common to both symmetric and asymmetric encryption
 * algorithms.
 *
 * @author Middleware Services
 * 
 * @version $Revision: 2745 $
 */
public interface EncryptionAlgorithm extends Algorithm {

	/**
	 * Gets the encryption mode.
	 *
	 * @return Name of an encryption mode, e.g. CBC.
	 */
	String getMode();

	/**
	 * Gets the encryption padding method.
	 *
	 * @return Name of a padding method, e.g. PKCS5Padding.
	 */
	String getPadding();

	/**
	 * Sets the key used for encryption/decryption.
	 *
	 * @param k Public, private, or secret key used for encryption or
	 *        decryption.
	 */
	void setKey( final Key k );

	/**
	 * Gets the cipher mode indicating whether this instance is currently
	 * initialized for encryption or decryption.
	 *
	 * @return <a href=
	 *         "https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html#ENCRYPT_MODE">Cipher.ENCRYPT_MODE</a>,
	 *         <a href=
	 *         "https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html#DECRYPT_MODE">Cipher.DECRYPT_MODE</a>,
	 *         or 0 if the cipher mode has not been initialized by calling
	 *         either {@link #initEncrypt()} or {@link #initDecrypt()}.
	 */
	int getCipherMode();

	/**
	 * Gets the block size of the encryption algorithm cipher in bytes.
	 *
	 * @return Block size of cipher in bytes, or 0 if the cipher is not a block
	 *         cipher.
	 */
	int getBlockSize();

	/**
	 * Initializes this instance for encryption operations.
	 *
	 * @throws CryptException On cryptographic configuration errors.
	 */
	void initEncrypt() throws CryptException;

	/**
	 * Initializes this instance for decryption operations.
	 *
	 * @throws CryptException On cryptographic configuration errors.
	 */
	void initDecrypt() throws CryptException;

	/**
	 * Encrypts the given plaintext bytes into a byte array of ciphertext using
	 * the encryption key.
	 *
	 * @param plaintext Input plaintext bytes.
	 *
	 * @return Ciphertext resulting from plaintext encryption.
	 *
	 * @throws CryptException On encryption errors.
	 */
	byte[] encrypt( final byte[] plaintext ) throws CryptException;

	/**
	 * Encrypts the given plaintext bytes into a ciphertext string using the
	 * conversion strategy provided by the given converter.
	 *
	 * @param plaintext Input plaintext bytes.
	 * @param converter Converter that converts ciphertext output bytes to a
	 *        string representation.
	 *
	 * @return Ciphertext string resulting from plaintext encryption.
	 *
	 * @throws CryptException On encryption errors.
	 */
	String encrypt( final byte[] plaintext, final Converter converter ) throws CryptException;

	/**
	 * Encrypts the data in the given plaintext input stream into ciphertext in
	 * the output stream. Use
	 * {@link edu.vt.middleware.crypt.io.Base64FilterOutputStream} or
	 * {@link edu.vt.middleware.crypt.io.HexFilterOutputStream} to produce
	 * ciphertext in the output stream in an encoded string repreprestation.
	 *
	 * @param in Input stream of plaintext.
	 * @param out Output stream of ciphertext.
	 *
	 * @throws CryptException On encryption errors.
	 * @throws IOException On stream read/write errors.
	 */
	void encrypt( final InputStream in, final OutputStream out ) throws CryptException, IOException;

	/**
	 * Decrypts the given ciphertext bytes into a byte array of plaintext using
	 * the decryption key.
	 *
	 * @param ciphertext Input ciphertext bytes.
	 *
	 * @return Plaintext resulting from ciphertext decryption.
	 *
	 * @throws CryptException On decryption errors.
	 */
	byte[] decrypt( final byte[] ciphertext ) throws CryptException;

	/**
	 * Decrypts a string representation of ciphertext bytes into a byte array of
	 * plaintext using the decryption key.
	 *
	 * @param ciphertext Input ciphertext bytes.
	 * @param converter Converter that converts the ciphertext input string into
	 *        raw bytes required by the cipher algorithm.
	 *
	 * @return Plaintext resulting from ciphertext decryption.
	 *
	 * @throws CryptException On decryption errors.
	 */
	byte[] decrypt( final String ciphertext, final Converter converter ) throws CryptException;

	/**
	 * Decrypts the data in the given ciphertext input stream into plaintext in
	 * the output stream. Use {@link Base64FilterInputStream} or
	 * {@link HexFilterInputStream} to consume ciphertext in an encoded string
	 * representation.
	 *
	 * @param in Input stream of ciphertext.
	 * @param out Output stream of plaintext.
	 *
	 * @throws CryptException On decryption errors.
	 * @throws IOException On stream read/write errors.
	 */
	void decrypt( final InputStream in, final OutputStream out ) throws CryptException, IOException;

}
