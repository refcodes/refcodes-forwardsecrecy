module org.refcodes.forwardsecrecy {
	requires org.bouncycastle.provider;
	requires org.bouncycastle.pkix;
	requires org.refcodes.controlflow;
	requires org.refcodes.data;
	requires org.refcodes.textual;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.generator;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.security;
	requires java.logging;
	requires jasypt;
	requires org.refcodes.runtime;

	exports org.refcodes.forwardsecrecy;
}
