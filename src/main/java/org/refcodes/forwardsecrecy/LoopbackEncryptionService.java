// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

/**
 * The {@link LoopbackEncryptionService} is an UNSECURE implementation of the
 * {@link EncryptionService} managing the ciphers in the {@link CipherVersion}
 * in plan text. This implementation's mere purpose is to provide means to
 * easily set up a quick and dirty test setup. The
 * {@link LoopbackDecryptionService} is the counterpart of the
 * {@link LoopbackEncryptionService} which both work (insecurely) hand in hand.
 * <p>
 * The {@link LoopbackEncryptionService} must not be used in production
 * environments!
 *
 * @deprecated To prevent accidental use in productive environment this insecure
 *             implementation of the {@link EncryptionService} has been marked
 *             as being deprecated; please use only for testing purposes.
 */
@Deprecated
public class LoopbackEncryptionService extends AbstractEncryptionService {

	/**
	 * {@inheritDoc}
	 * 
	 * @deprecated To prevent accidental use in productive environment this
	 *             insecure implementation of the {@link EncryptionService} has
	 *             been marked as being deprecated; please use only for testing
	 *             purposes.
	 */
	@Deprecated
	public LoopbackEncryptionService( String aNamespace, EncryptionServer aEncryptionServer ) {
		super( aNamespace, aEncryptionServer, new CipherVersionGeneratorImpl() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOK METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	protected <CV extends CipherVersion> CV toEncryptedCipherVersion( CV aDecyrptedCipherVersion ) {
		return aDecyrptedCipherVersion;
	}
}
