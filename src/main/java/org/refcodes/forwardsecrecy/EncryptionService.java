// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

/**
 * An {@link EncryptionService} is bound to a single namespace and creates
 * unique {@link CipherVersion} instances for that namespace.
 * <p>
 * The {@link EncryptionService} may make use of an encryption server persisting
 * {@link CipherVersion} instances per namespace. It could actually generate a
 * dedicated cipher just once, so any unauthorized system having access to the
 * ciphers gets a different cipher not used by any of the authorized
 * participant. Never two participants will encrypt with the same cipher (taken
 * the probability that two participants generate the same cipher is very low
 * and nearly never to happen; in case it happens there is still no security
 * risk). The key advantage is that if an intruder can also retrieve ciphers,
 * those ciphers being retrieved are never used by other systems for encryption
 * as a cipher version is bound to the requester.
 * <p>
 * To later determine which cipher to use when decrypting data, each cipher has
 * a cipher UDI assigned to it (a cipher UID and cipher make up a
 * {@link CipherVersion}). Encrypted data is prefixed with this cipher UID so
 * later it is easy to determine which cipher is responsible for decryption. The
 * cipher UID is assumed to be public as it's generation must be completely
 * independent from the cipher itself. Unauthorized systems having access to the
 * cipher UID cannot reverse calculate the cipher
 * <p>
 * There is not even a relation between cipher and cipher UID in terms of hash
 * code. This means using brute force approaches with rainbow tables or
 * whatsoever to reconstruct the cipher from the cipher UID is to fail.
 * <p>
 * Depending on the implementation, the {@link EncryptionService} makes use of a
 * public key of an asymmetric encryption approach for encrypting the
 * {@link CipherVersion} instances; to be persisted by the encryption server.
 * <p>
 * Encrypting only the cipher is sufficient, the cipher UID can be stored in
 * plain text; it securely can be assumed to be public. As said before, any
 * intruder knowing the cipher UIDs does not weaken the forward secrecy
 * cryptography infrastructure as knowing the cipher UIDs is only of use with
 * the according ciphers; which cannot be calculated from the cipher UIDs.
 */
public interface EncryptionService extends CipherVersionGenerator<CipherVersion> {}
