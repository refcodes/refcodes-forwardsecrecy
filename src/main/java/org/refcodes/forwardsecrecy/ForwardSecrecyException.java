// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.mixin.NamespaceAccessor;
import org.refcodes.mixin.UniversalIdAccessor;
import org.refcodes.security.SecurityException;

/**
 * The Class {@link ForwardSecrecyException}.
 */
public abstract class ForwardSecrecyException extends SecurityException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * The Class {@link ForwardSecrecyCipherException}.
	 */
	protected abstract static class ForwardSecrecyCipherException extends ForwardSecrecyException implements UniversalIdAccessor, NamespaceAccessor {

		private static final long serialVersionUID = 1L;

		protected String _uId;
		protected String _namespace;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aUid The universal ID involved in this exception.
		 * @param aNamespace The namespace involved in this exception.
		 */
		public ForwardSecrecyCipherException( String aMessage, String aNamespace, String aUid, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_uId = aUid;
			_namespace = aNamespace;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aNamespace The namespace involved in this exception.
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyCipherException( String aMessage, String aNamespace, String aUid, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_uId = aUid;
			_namespace = aNamespace;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aNamespace The namespace involved in this exception.
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyCipherException( String aMessage, String aNamespace, String aUid, Throwable aCause ) {
			super( aMessage, aCause );
			_uId = aUid;
			_namespace = aNamespace;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aNamespace The namespace involved in this exception.
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyCipherException( String aMessage, String aNamespace, String aUid ) {
			super( aMessage );
			_uId = aUid;
			_namespace = aNamespace;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aNamespace The namespace involved in this exception.
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyCipherException( String aNamespace, String aUid, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_uId = aUid;
			_namespace = aNamespace;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aNamespace The namespace involved in this exception.
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyCipherException( String aNamespace, String aUid, Throwable aCause ) {
			super( aCause );
			_uId = aUid;
			_namespace = aNamespace;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getUniversalId() {
			return _uId;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getNamespace() {
			return _namespace;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _namespace, _uId };
		}
	}
}
