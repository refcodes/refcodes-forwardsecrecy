// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.util.List;

/**
 * A {@link DecryptionService} is bound to a single namespace and provides
 * cipher versions required for decrypting text by the
 * {@link DecryptionProvider}.
 * <p>
 * The {@link DecryptionService} may make use of a {@link DecryptionServer}
 * managing the {@link CipherVersion} instances per namespace.
 * <p>
 * Depending on the implementation, the {@link DecryptionService} has a private
 * key for an asymmetric encryption approach whose public counterpart is used by
 * the {@link EncryptionService}. This private key then is used to decrypt the
 * ciphers form the retrieved {@link CipherVersion} instances.
 * <p>
 * A {@link DecryptionServer}'s wrapper may be hooked on top of the
 * {@link DecryptionServer} containing public keys known as being trusted and
 * the private key for decrypting ciphers being encrypted by the
 * {@link EncryptionService}. When cipher versions are being requested by a
 * {@link DecryptionService} from the wrapped {@link DecryptionServer}, the
 * {@link DecryptionService} authorizes itself by signing a aMessage with a
 * signature passed to the {@link DecryptionServer}. In case the aMessage's
 * signature is verified by the {@link DecryptionServer} with one of its trusted
 * public keys, then the public key in question is used by the
 * {@link DecryptionServer} for encrypting the {@link CipherVersion} instances
 * being transmitted to the {@link DecryptionService}.
 */
public interface DecryptionService {

	/**
	 * Returns the currently available cipher versions.
	 * 
	 * @return A list with the currently known cipher versions.
	 */
	List<CipherVersion> getCipherVersions();
}
