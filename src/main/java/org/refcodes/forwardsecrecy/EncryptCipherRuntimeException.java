// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.forwardsecrecy.ForwardSecrecyRuntimeException.ForwardSecrecyUidRuntimeException;

/**
 * The Class {@link EncryptCipherRuntimeException}.
 */
public class EncryptCipherRuntimeException extends ForwardSecrecyUidRuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public EncryptCipherRuntimeException( String aMessage, String aUid, String aErrorCode ) {
		super( aMessage, aUid, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public EncryptCipherRuntimeException( String aMessage, String aUid, Throwable aCause, String aErrorCode ) {
		super( aMessage, aUid, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public EncryptCipherRuntimeException( String aMessage, String aUid, Throwable aCause ) {
		super( aMessage, aUid, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public EncryptCipherRuntimeException( String aMessage, String aUid ) {
		super( aMessage, aUid );
	}

	/**
	 * {@inheritDoc}
	 */
	public EncryptCipherRuntimeException( String aUid, Throwable aCause, String aErrorCode ) {
		super( aUid, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public EncryptCipherRuntimeException( String aUid, Throwable aCause ) {
		super( aUid, aCause );
	}
}
