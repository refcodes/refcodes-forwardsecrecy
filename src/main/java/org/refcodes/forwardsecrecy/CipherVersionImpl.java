// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.io.Serializable;

/**
 * The Class CipherVersionImpl.
 *
 * @see CipherVersion
 */
public class CipherVersionImpl implements CipherVersion, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _uid;

	private final String _cipher;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the data structure with the required attributes.
	 * 
	 * @param aCipherUid The UID identifying the cipher.
	 * @param aCipher The cipher related to the given UID
	 */
	public CipherVersionImpl( String aCipherUid, String aCipher ) {
		_uid = aCipherUid;
		_cipher = aCipher;
	}

	// /////////////////////////////////////////////////////////////////////////
	// ATTRIBUTES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUniversalId() {
		return _uid;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCipher() {
		return _cipher;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo( CipherVersion aCipherVersion ) {
		return _uid.compareTo( aCipherVersion.getUniversalId() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _uid == null ) ? 0 : _uid.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final CipherVersionImpl other = (CipherVersionImpl) obj;
		if ( _uid == null ) {
			if ( other._uid != null ) {
				return false;
			}
		}
		else if ( !_uid.equals( other._uid ) ) {
			return false;
		}
		return true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

}
