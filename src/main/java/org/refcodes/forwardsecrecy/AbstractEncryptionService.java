// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.RetryCount;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.data.Text;
import org.refcodes.exception.Trap;

/**
 * Abstract base implementation for non abstract {@link EncryptionService}
 * implementations.
 */
public abstract class AbstractEncryptionService implements EncryptionService {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( AbstractEncryptionService.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final CipherVersionGenerator<CipherVersion> _cipherVersionGenerator;

	private final String _namespace;

	private final EncryptionServer _encryptionServer;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the service with the required services and configuration.
	 * 
	 * @param aNamespace The name space to which this service belongs
	 * @param aEncryptionServer The server to which the service is being
	 *        "connected"
	 * @param aCipherVersionGenerator The generator to be used for generating
	 *        {@link CipherVersion} instances.
	 */
	public AbstractEncryptionService( String aNamespace, EncryptionServer aEncryptionServer, CipherVersionGenerator<CipherVersion> aCipherVersionGenerator ) {
		_cipherVersionGenerator = aCipherVersionGenerator;
		_encryptionServer = aEncryptionServer;
		_namespace = aNamespace;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CipherVersion next() {
		CipherVersion eCipherVersion = null;
		final RetryCounter theRetryCounter = new RetryCounter( RetryCount.NORM.getValue(), SleepLoopTime.MIN.getTimeMillis() );
		while ( theRetryCounter.nextRetry() ) {
			try {
				eCipherVersion = _cipherVersionGenerator.next();

				// -------------------------------------------------------------
				// Hotspot for persisting the cipher in a DB or a storage using
				// some kind of asymmetric encryption mechanism
				// -------------------------------------------------------------
				_encryptionServer.addCipherVersion( _namespace, toEncryptedCipherVersion( eCipherVersion ) );
				return eCipherVersion;
			}
			catch ( CipherUidAlreadyInUseException | IOException e ) {
				if ( !theRetryCounter.hasNextRetry() ) {
					LOGGER.log( Level.WARNING, "Failed to add a cipher UID \"" + eCipherVersion.getUniversalId() + "\", retry count is <" + theRetryCounter.getRetryCount() + "> of <" + theRetryCounter.getRetryNumber() + "> (waiting for <" + ( theRetryCounter.getNextRetryDelayMillis() / 1000 ) + "> seconds before next retry) as of: " + Trap.asMessage( e ), e );
				}
			}
		}
		throw new UnexpectedForwardSecrecyRuntimeException( "Unable to generate a Chipher-Version within <" + theRetryCounter.getRetryNumber() + "> tries, aborting as else we might loop infinte time!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException( Text.UNSUPPORTED_OPERATION.getText() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOK METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook factory method to be implemented by subclasses. The provided cipher
	 * is to be encrypted (e.g. with the public key of an asymmetric encryption
	 * approach) so that an Encryption-Server only receives encrypted data.
	 * 
	 * @param <CV> The type of the {@link CipherVersion} to be used.
	 * @param aDecyrptedCipherVersion The {@link CipherVersion} to be encrypted.
	 * 
	 * @return The encrypted {@link CipherVersion}.
	 */
	protected abstract <CV extends CipherVersion> CV toEncryptedCipherVersion( CV aDecyrptedCipherVersion );
}
