// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.security.Decrypter;
import org.refcodes.security.DecryptionException;

/**
 * The {@link DecryptionProvider} is bound to a namespace and is being used by
 * the business logic to decrypt data.
 * <p>
 * The {@link DecryptionProvider} provides decrypting functionality as encrypted
 * data must be decrypted again by another service or system. This system must
 * now be able to retrieve all known ciphers versions (by a
 * {@link DecryptionService}) for determining the correct cipher for decrypting
 * encrypted text (as encrypted text is prefixed by the cipher UID identifying
 * the cipher to use for decryption).
 */
public interface DecryptionProvider extends Decrypter<String, String, DecryptionException> {

	/**
	 * Encrypts a text with the latest known valid cipher.
	 * 
	 * @param aEncryptedText The text to be encrypted
	 * 
	 * @return The encrypted text with a version prefixed identifying the cipher
	 *         being used for encryption.
	 * 
	 * @throws UnknownCipherUidException in case the cipher UID prefixed to the
	 *         encrypted text is unknown
	 * @throws NoCipherUidException in case no cipher UID was found prefixed to
	 *         the provided text.
	 */
	@Override
	String toDecrypted( String aEncryptedText ) throws UnknownCipherUidException, NoCipherUidException;
}
