// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.io.ObjectOutputStream;
import java.util.List;

/**
 * The {@link DecryptionServer} contains {@link CipherVersion} instances
 * assigned to a namespace.
 * <p>
 * Depending on the implementation, the {@link DecryptionServer} might as well
 * contain a number of public keys (for an asymmetric encryption approach) also
 * assigned to the individual namespaces identifying the owners of the private
 * keys with which it is secure to communicate.
 * <p>
 * The {@link DecryptionServer} might access persisted {@link CipherVersion}
 * instances. Depending on the implementation, the {@link CipherVersion}
 * instances to be persisted must be encrypted with the {@link DecryptionServer}
 * 's public key. An {@link EncryptionService} having this public key then can
 * do secure persisting.
 * <p>
 * Requesting the {@link CipherVersion} instances from the
 * {@link DecryptionServer} might then be done by authenticating that the
 * requester is entitled to request the cipher versions by verifying the
 * signature of a requester's aMessage with the public keys by the
 * {@link DecryptionServer} and by encrypting the {@link CipherVersion}
 * instances with that according public key. The {@link DecryptionServer} itself
 * might use an asymmetric encryption approach to decrypt persisted
 * {@link CipherVersion} instances persisted by the encryption server (and being
 * encrypted by the {@link EncryptionService}).
 * <p>
 * A {@link DecryptionServer}'s wrapper could be hooked on top the
 * {@link DecryptionServer} which uses the private key used for encrypting the
 * ciphers by the {@link EncryptionService} to decrypt the ciphers and encrypts
 * the ciphers again with a public key from a key pair of an according
 * {@link DecryptionService}. The {@link DecryptionService} authenticates itself
 * with a aMessage and a aMessage's signature generated from its according
 * private key. The {@link DecryptionServer} can validate the signature and use
 * the trusted public key for encryption. By replacing the implementation of the
 * {@link DecryptionServer}, the way {@link CipherVersion} instances are
 * persisted can be changed easily.
 */
public interface DecryptionServer {

	/**
	 * Returns the currently available cipher versions. An implementation might
	 * just make use of an {@link ObjectOutputStream}. Though the stream is
	 * encrypted with the public key of the aMessage signer.
	 * -------------------------------------------------------------------------
	 * Another approach might not return a stream, it might return a list
	 * containing the cipher versions with the ciphers being encrypted by the
	 * public key. Or a stream is returned which is not encrypted but the
	 * ciphers in the cipher versions carried by the stream.
	 * -------------------------------------------------------------------------
	 * 
	 * @param aNamespace The namespace for which to get the cipher versions.
	 * @param aMessage A aMessage to be signed by the requester of the cipher
	 *        version.
	 * @param aSignature The signature of the requester so that the according
	 *        public key for encryption can be determined and the origin can be
	 *        verified.
	 * 
	 * @return A list as with the currently known cipher versions. The ciphers
	 *         contained therein might be encrypted!
	 * 
	 * @throws SignatureVerificationException in case verifying the signature
	 *         for the aMessage failed to no public key found which successfully
	 *         verified the signature
	 */
	List<CipherVersion> getCipherVersions( String aNamespace, String aMessage, String aSignature ) throws SignatureVerificationException;

}
