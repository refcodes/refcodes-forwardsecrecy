// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link InMemoryDecryptionServer} is a non-persisting implementation of
 * the {@link DecryptionServer} managing the {@link CipherVersion} instances in
 * memory only. This implementation provides means to easily set up a quick and
 * dirty test setup. The {@link InMemoryDecryptionServer} is the counterpart of
 * the {@link InMemoryEncryptionServer} which both work (not doing any
 * persistence) hand in hand.
 */
public class InMemoryDecryptionServer implements DecryptionServer {

	private final Map<String, List<CipherVersion>> _namespaceToCipherVersions = new HashMap<>();

	private final CipherVersionFactory<CipherVersion> _cipherVersionFactory = new CipherVersionFactoryImpl();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CipherVersion> getCipherVersions( String aNamespace, String aMessage, String aSignature ) {
		return getCipherVersions( aNamespace );
	}

	// /////////////////////////////////////////////////////////////////////////
	// IMPLEMENTATION SPECIFIC:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Adds a cipher version to the server. Bad hack for testing purposes.
	 *
	 * @param aNamespace The namespace for which to add the cipher
	 * @param aCipherVersion The cipher version with cipher UID and cipher.
	 * 
	 * @throws CipherUidAlreadyInUseException in case the given cipher UID has
	 *         already been used.
	 */
	protected void addCipherVersion( String aNamespace, CipherVersion aCipherVersion ) throws CipherUidAlreadyInUseException {
		final List<CipherVersion> theCipherVersions = getCipherVersions( aNamespace );
		final CipherVersion theCipherVersion = _cipherVersionFactory.create( aCipherVersion.getUniversalId(), aCipherVersion.getCipher() );
		if ( theCipherVersions.contains( theCipherVersion ) ) {
			throw new CipherUidAlreadyInUseException( "A cipher version with the UID <{1}> of namespace <{0}> is already in use!", aNamespace, theCipherVersion.getUniversalId() );
		}
		theCipherVersions.add( theCipherVersion );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Lazy getting cipher versions.
	 * 
	 * @param aNameSsace The namespace for which to get the
	 *        {@link CipherVersion}s
	 * 
	 * @return The {@link CipherVersion}s or an empty list in case none have
	 *         been created for the given namespace yet.
	 */
	private List<CipherVersion> getCipherVersions( String aNameSsace ) {
		List<CipherVersion> theCipherVersions = _namespaceToCipherVersions.get( aNameSsace );
		if ( theCipherVersions == null ) {
			synchronized ( this ) {
				theCipherVersions = _namespaceToCipherVersions.get( aNameSsace );
				if ( theCipherVersions == null ) {
					theCipherVersions = new ArrayList<>();
					_namespaceToCipherVersions.put( aNameSsace, theCipherVersions );
				}
			}
		}
		return theCipherVersions;
	}
}
