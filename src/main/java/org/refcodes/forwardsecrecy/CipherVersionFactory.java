// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.factory.ContextLookupFactory;

/**
 * As requirements might arise to use {@link CipherVersion} instances with
 * additional attributes or functionality; the {@link CipherVersionFactory} can
 * be replaced with a custom implementation instantiating {@link CipherVersion}
 * (sub-)types with the additional required attributes or functionality.
 * Additional attributes might be a validity date useful for housekeeping or
 * management purposes.
 * <p>
 * In case you provide your custom {@link CipherVersionFactory} implementation,
 * make sure the cipher version (sub-)type you return fits with the
 * {@link CipherVersion} (sub-)type of your custom
 * {@link CipherVersionGenerator}. A good approach is to make your custom
 * {@link CipherVersionGenerator} make use your custom
 * {@link CipherVersionFactory}.
 * <p>
 * See the default implementations {@link CipherVersionGeneratorImpl}, the
 * {@link AbstractCipherVersionGenerator} and the
 * {@link CipherVersionFactoryImpl}.
 *
 * @param <CV> The type of the {@link CipherVersion} to be created.
 */
public interface CipherVersionFactory<CV extends CipherVersion> extends ContextLookupFactory<CV, String, String> {

	/**
	 * Factory method to be implemented by subclasses. Required to create
	 * implementation dependent {@link CipherVersion} objects.
	 * 
	 * @param aCipherUid The UID for the cipher to be stored in the
	 *        {@link CipherVersion}
	 * @param aCipher The cipher to be stored in the {@link CipherVersion}.
	 * 
	 * @return The {@link CipherVersion} with the cipher UID and the cipher
	 */
	@Override
	CV create( String aCipherUid, String aCipher );
}
