// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.security.Encrypter;
import org.refcodes.security.EncryptionException;

/**
 * The {@link EncryptionProvider} is bound to a namespace and is being used by
 * the business logic to encrypt data.
 * <p>
 * It is merely responsible for retrieving a currently valid cipher for
 * encrypting data. The {@link EncryptionProvider} does not expose the cipher
 * though it might store it in clear text in-memory only (advanced
 * implementations might encrypt the in-memory cipher). The (internally)
 * retrieved cipher is requested from and provided by an
 * {@link EncryptionService} (on the same machine) which takes care of providing
 * (and creating) a cipher with a cipher UID as well as publishing the according
 * {@link CipherVersion} (via the {@link EncryptionServer} to the
 * {@link DecryptionServer}). As the {@link EncryptionProvider} does not persist
 * any data, an in-memory {@link CipherVersion} will only be used as long as the
 * {@link EncryptionProvider} is up-and-running. In case of a restart, a new
 * cipher for encryption is requested from the {@link EncryptionService}. Also
 * the encryption provider can be forced to (create and to) use a next valid
 * cipher on demand.
 */
public interface EncryptionProvider extends Encrypter<String, String, EncryptionException> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	String toEncrypted( String aText ) throws EncryptionException;

	/**
	 * Forces a next valid cipher to be requested and used.
	 */
	void nextCipherVersion();
}
