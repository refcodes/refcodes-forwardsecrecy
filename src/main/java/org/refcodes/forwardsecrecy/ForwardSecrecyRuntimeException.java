// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.exception.AbstractRuntimeException;
import org.refcodes.mixin.UniversalIdAccessor;

/**
 * The Class {@link ForwardSecrecyRuntimeException}.
 */
public abstract class ForwardSecrecyRuntimeException extends AbstractRuntimeException {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyRuntimeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyRuntimeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ForwardSecrecyRuntimeException( Throwable aCause ) {
		super( aCause );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class {@link ForwardSecrecyUidRuntimeException}.
	 */
	protected abstract static class ForwardSecrecyUidRuntimeException extends ForwardSecrecyRuntimeException implements UniversalIdAccessor {

		private static final long serialVersionUID = 1L;

		protected String _uId;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyUidRuntimeException( String aMessage, String aUid, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_uId = aUid;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyUidRuntimeException( String aMessage, String aUid, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_uId = aUid;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyUidRuntimeException( String aMessage, String aUid, Throwable aCause ) {
			super( aMessage, aCause );
			_uId = aUid;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyUidRuntimeException( String aMessage, String aUid ) {
			super( aMessage );
			_uId = aUid;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyUidRuntimeException( String aUid, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_uId = aUid;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aUid The universal ID involved in this exception.
		 */
		public ForwardSecrecyUidRuntimeException( String aUid, Throwable aCause ) {
			super( aCause );
			_uId = aUid;
		}

		/**
		 * {@inheritDoc}n
		 */
		@Override
		public String getUniversalId() {
			return _uId;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _uId };
		}
	}
}
