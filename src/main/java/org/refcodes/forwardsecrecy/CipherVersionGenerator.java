// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.generator.Generator;

/**
 * Depending on the security demands and performance issues; the
 * {@link Generator} generating {@link CipherVersion} instances can be replaced
 * with a custom {@link CipherVersionGenerator} using its own approach
 * generating ciphers and cipher UIDs ({@link CipherVersion} instances).
 * <p>
 * In case you provide your custom {@link CipherVersionGenerator}
 * implementation, make sure the {@link CipherVersion} (sub-)type you return
 * fits with the {@link CipherVersion} (sub-)type of your custom
 * {@link CipherVersionFactory}. A good approach is to make your custom
 * {@link CipherVersionGenerator} make use your custom
 * {@link CipherVersionFactory}.
 * <p>
 * See the default implementations {@link CipherVersionGeneratorImpl}, the
 * {@link AbstractCipherVersionGenerator} and the
 * {@link CipherVersionFactoryImpl}.
 *
 * @param <CV> The type of the {@link CipherVersion} to be generated.
 */
public interface CipherVersionGenerator<CV extends CipherVersion> extends Generator<CV> {

	/**
	 * Generator method to be implemented by subclasses. Required to create
	 * implementation dependent cipher version objects. Retrieves the next valid
	 * cipher which may be used for encrypting data.
	 * 
	 * @return The next valid {@link CipherVersion} for encryption.
	 */
	@Override
	CV next();

}
