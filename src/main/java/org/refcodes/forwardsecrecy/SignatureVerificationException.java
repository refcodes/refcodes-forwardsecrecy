// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

/**
 * The Class {@link SignatureVerificationException}.
 */
public class SignatureVerificationException extends SignatureException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public SignatureVerificationException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public SignatureVerificationException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public SignatureVerificationException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public SignatureVerificationException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public SignatureVerificationException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public SignatureVerificationException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * The Class {@link SignatureVerificationRuntimeException}.
	 */
	public static class SignatureVerificationRuntimeException extends SignatureRuntimeException {

		private static final long serialVersionUID = 1L;

		/**
		 * {@inheritDoc}
		 */
		public SignatureVerificationRuntimeException( String aMessage, String aErrorCode ) {
			super( aMessage, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public SignatureVerificationRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public SignatureVerificationRuntimeException( String aMessage, Throwable aCause ) {
			super( aMessage, aCause );
		}

		/**
		 * {@inheritDoc}
		 */
		public SignatureVerificationRuntimeException( String aMessage ) {
			super( aMessage );
		}

		/**
		 * {@inheritDoc}
		 */
		public SignatureVerificationRuntimeException( Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public SignatureVerificationRuntimeException( Throwable aCause ) {
			super( aCause );
		}
	}
}
