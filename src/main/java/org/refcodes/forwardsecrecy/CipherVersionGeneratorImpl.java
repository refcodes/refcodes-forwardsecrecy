// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

/**
 * Implementation if the {@link AbstractCipherVersionGenerator} for the usage of
 * plain {@link CipherVersion} instances.
 */
public class CipherVersionGeneratorImpl extends AbstractCipherVersionGenerator<CipherVersion> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new cipher version generator impl.
	 *
	 * @param aCipVersionFactory the cip version factory
	 */
	public CipherVersionGeneratorImpl( CipherVersionFactory<CipherVersion> aCipVersionFactory ) {
		super( aCipVersionFactory );
	}

	/**
	 * Constructs a {@link CipherVersionGenerator} using the
	 * {@link CipherVersionFactoryImpl} for creating its {@link CipherVersion}
	 * instances.
	 */
	public CipherVersionGeneratorImpl() {
		super( new CipherVersionFactoryImpl() );
	}
}
