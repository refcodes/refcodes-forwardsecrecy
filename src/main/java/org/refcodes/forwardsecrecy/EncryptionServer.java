// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.io.IOException;

/**
 * The {@link EncryptionServer} takes {@link CipherVersion} instances generated
 * by an {@link EncryptionService} for a provided namespace. A
 * {@link CipherVersion} provided to the encryption server (by an
 * {@link EncryptionService}) is persisted so that the {@link DecryptionServer}
 * can access this {@link CipherVersion}. When persisting, a
 * {@link CipherVersion}'s cipher UID is verified whether there is already a
 * {@link CipherVersion} with the same cipher UID already persisted and where
 * necessary rejected (such collisions can be avoided with good cipher UIDs).
 * <p>
 * Regarding the implementation of the {@link EncryptionServer}, securely
 * persisting can be done with the public key of an asymmetric encryption
 * approach so that only the {@link DecryptionService} can get the plain text
 * ciphers from the {@link CipherVersion} instances. To avoid transmitting plain
 * text {@link CipherVersion} instances from the {@link EncryptionService} to
 * the {@link EncryptionServer}, the {@link EncryptionService} should already
 * encrypt the {@link CipherVersion} with the according public key so that the
 * {@link EncryptionServer} always receives encrypted {@link CipherVersion}
 * instances.
 * <p>
 * The forward secrecy cryptography infrastructure supports
 * {@link EncryptionServer} instances which only need to take care of persisting
 * the {@link CipherVersion} instances and retrieving them. Encryption and
 * decryption can be done in the according service layers. E.g. the
 * {@link EncryptionService} uses a public key to encrypt the cipher of a
 * {@link CipherVersion} instances and passes it to the {@link EncryptionServer}
 * just storing the {@link CipherVersion} without any additional encryption. A
 * {@link DecryptionService} in turn requests the {@link CipherVersion}
 * instances with the encrypted ciphers from the {@link DecryptionServer} and is
 * decrypting the ciphers with the according private key. Another more complex
 * approach is described regarding the {@link DecryptionServer}.
 * <p>
 * By replacing the implementation of the {@link EncryptionServer}, the way
 * {@link CipherVersion} instances are persisted can be changed easily.
 */
public interface EncryptionServer {

	/**
	 * Adds a {@link CipherVersion} to the {@link EncryptionServer}.
	 * 
	 * @param aNamespace The namespace for which to add the cipher
	 * @param aCipherVersion The {@link CipherVersion} to be added.
	 * 
	 * @throws CipherUidAlreadyInUseException in case the given cipher UID has
	 *         already been used.
	 * 
	 * @throws IOException in case I/O related problems were encountered.
	 */
	void addCipherVersion( String aNamespace, CipherVersion aCipherVersion ) throws CipherUidAlreadyInUseException, IOException;
}
