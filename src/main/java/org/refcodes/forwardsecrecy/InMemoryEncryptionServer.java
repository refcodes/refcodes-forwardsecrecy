// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

/**
 * The {@link InMemoryEncryptionServer} is a non-persisting implementation of
 * the {@link EncryptionServer} managing the {@link CipherVersion} instances in
 * memory only. This implementation provides means to easily set up a quick and
 * dirty test setup. The {@link InMemoryDecryptionServer} is the counterpart of
 * the {@link InMemoryEncryptionServer} which both work (not doing any
 * persistence) hand in hand.
 */
public class InMemoryEncryptionServer implements EncryptionServer {

	private final InMemoryDecryptionServer _decryptionServer;

	/**
	 * Constructs the server with the provided decryption server.
	 * 
	 * @param aDecryptionServer The decryption server to be used simulating a
	 *        cipher repository of some kind
	 */
	public InMemoryEncryptionServer( InMemoryDecryptionServer aDecryptionServer ) {
		_decryptionServer = aDecryptionServer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addCipherVersion( String aNamespace, CipherVersion aCipherVersion ) throws CipherUidAlreadyInUseException {
		_decryptionServer.addCipherVersion( aNamespace, aCipherVersion );
	}
}
