// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

/**
 * The {@link LoopbackDecryptionService} is an UNSECURE implementation of the
 * {@link DecryptionService} managing the ciphers in the {@link CipherVersion}
 * in plan text. This implementation's mere purpose is to provide means to
 * easily set up a quick and dirty test setup. The
 * {@link LoopbackDecryptionService} is the counterpart of the
 * {@link LoopbackEncryptionService} which both work (insecurely) hand in hand.
 * <p>
 * The {@link LoopbackDecryptionService} must not be used in production
 * environments!
 *
 * @deprecated To prevent accidental use in productive environment this insecure
 *             implementation of the {@link DecryptionService} has been marked
 *             as being deprecated; please use only for testing purposes.
 */
@Deprecated
public class LoopbackDecryptionService extends AbstractDecryptionService {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @deprecated To prevent accidental use in productive environment this
	 *             insecure implementation of the {@link DecryptionService} has
	 *             been marked as being deprecated; please use only for testing
	 *             purposes.
	 */
	@Deprecated
	public LoopbackDecryptionService( String aNamespace, InMemoryDecryptionServer aDecryptionServer ) {
		this( aNamespace, aDecryptionServer, EXPIRE_TIME_IMMEDIATELY );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aCipherVersionsExpireTimeMillis The time in milliseconds after
	 *        which them loaded cipher versions expire and are reloaded. A value
	 *        of 0 indicates that them cipher versions expire immediately
	 *        (default). A value of -1 indicate that them cipher versions expire
	 *        never.
	 * 
	 * 
	 * @deprecated To prevent accidental use in productive environment this
	 *             insecure implementation of the {@link DecryptionService} has
	 *             been marked as being deprecated; please use only for testing
	 *             purposes.
	 */
	@Deprecated
	public LoopbackDecryptionService( String aNamespace, InMemoryDecryptionServer aDecryptionServer, long aCipherVersionsExpireTimeMillis ) {
		super( aNamespace, aDecryptionServer, aCipherVersionsExpireTimeMillis );
	}

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final RandomTextGenerartor _rndTextGenerator = new RandomTextGenerartor().withColumnWidth( 64 ).withRandomTextMode( RandomTextMode.ASCII );

	// /////////////////////////////////////////////////////////////////////////
	// HOOK METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String toSignature( String aMessage ) {
		return aMessage;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String createMessage() {
		return _rndTextGenerator.next();
	}

	/**
	 * {@inheritDoc} CAUTION: The encrypted {@link CipherVersion} argument is
	 * returned unmodified (not decrypted!). Please overwrite this method with a
	 * decrypting one!
	 * 
	 * @deprecated To prevent accidental use in productive environment this
	 *             insecure implementation of the {@link DecryptionService} has
	 *             been marked as being deprecated; please use only for testing
	 *             purposes.
	 */
	@Deprecated
	@Override
	protected <CV extends CipherVersion> CV toDecryptedCipherVersion( CV aEncryptedCipherVersion ) {
		return aEncryptedCipherVersion;
	}
}