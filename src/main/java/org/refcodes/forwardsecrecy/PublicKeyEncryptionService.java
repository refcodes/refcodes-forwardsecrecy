package org.refcodes.forwardsecrecy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.exception.Trap;

import edu.vt.middleware.crypt.CryptException;
import edu.vt.middleware.crypt.asymmetric.AsymmetricAlgorithm;
import edu.vt.middleware.crypt.asymmetric.RSA;
import edu.vt.middleware.crypt.util.Base64Converter;

/**
 * This Encryption-Service uses a public key for an asymmetric encryption
 * algorithm in order to encrypt any ciphers being generated before them ciphers
 * are passed to the Encryption-Server which persists them cipher versions with
 * the cipher UID and the encrypted cipher. A decryption server may retrieve the
 * cipher versions with the cipher UID and the encrypted cipher and pass it to a
 * Decryption-Service which might decrypt the ciphers with an according private
 * key.
 */
public class PublicKeyEncryptionService extends AbstractEncryptionService {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( PublicKeyEncryptionService.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private CipherVersionFactory<CipherVersion> _cipherVersionFactory;
	private final AsymmetricAlgorithm _encryptAlgorithm = new RSA();
	private final Base64Converter _base64Converter = new Base64Converter();
	private String _publicKeyPath; /* For logging only */

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the service with the required services and configuration.
	 *
	 * @param aNamespace The name space to which this service belongs
	 * @param aPublicKeyPath The path on the file system to the public key to be
	 *        used for encrypting any ciphers passed to the Encryption-Server
	 *        for persisting.
	 * @param aEncryptionServer The server to which the service is being
	 *        "connected"
	 * 
	 * @throws CryptException in case a crypt exception occurred when
	 *         instantiating the public key
	 * @throws IOException in case reading the file public key caused IO
	 *         problems
	 * @throws GeneralSecurityException in case a security poroblem was
	 *         encountered.
	 */
	public PublicKeyEncryptionService( String aNamespace, String aPublicKeyPath, EncryptionServer aEncryptionServer ) throws CryptException, IOException, GeneralSecurityException {
		this( aNamespace, aPublicKeyPath, aEncryptionServer, new CipherVersionGeneratorImpl(), new CipherVersionFactoryImpl() );
	}

	/**
	 * Constructs the service with the required services and configuration.
	 *
	 * @param aNamespace The name space to which this service belongs
	 * @param aPublicKeyPath The path on the file system to the public key to be
	 *        used for encrypting any ciphers passed to the Encryption-Server
	 *        for persisting.
	 * @param aEncryptionServer The server to which the service is being
	 *        "connected"
	 * @param aCipherVersionGenerator The generator to be used for generating
	 *        {@link CipherVersion} instances.
	 * 
	 * @throws CryptException in case a crypt exception occurred when
	 *         instantiating the public key
	 * @throws IOException in case reading the file public key caused IO
	 *         problems
	 * @throws GeneralSecurityException in case a security poroblem was
	 *         encountered.
	 */
	public PublicKeyEncryptionService( String aNamespace, String aPublicKeyPath, EncryptionServer aEncryptionServer, CipherVersionGenerator<CipherVersion> aCipherVersionGenerator ) throws CryptException, IOException, GeneralSecurityException {
		this( aNamespace, aPublicKeyPath, aEncryptionServer, aCipherVersionGenerator, new CipherVersionFactoryImpl() );
	}

	/**
	 * Constructs the service with the required services and configuration.
	 *
	 * @param aNamespace The name space to which this service belongs
	 * @param aPublicKeyPath The path on the file system to the public key to be
	 *        used for encrypting any ciphers passed to the Encryption-Server
	 *        for persisting.
	 * @param aEncryptionServer The server to which the service is being
	 *        "connected"
	 * @param aCipherVersionFactory The factory to be used for creating
	 *        {@link CipherVersion} instances.
	 * 
	 * @throws CryptException in case a crypt exception occurred when
	 *         instantiating the public key
	 * @throws IOException in case reading the file public key caused IO
	 *         problems
	 * @throws GeneralSecurityException in case a security poroblem was
	 *         encountered.
	 */
	public PublicKeyEncryptionService( String aNamespace, String aPublicKeyPath, EncryptionServer aEncryptionServer, CipherVersionFactory<CipherVersion> aCipherVersionFactory ) throws CryptException, IOException, GeneralSecurityException {
		this( aNamespace, aPublicKeyPath, aEncryptionServer, new CipherVersionGeneratorImpl(), aCipherVersionFactory );
	}

	/**
	 * Constructs the service with the required services and configuration.
	 *
	 * @param aNamespace The name space to which this service belongs
	 * @param aPublicKeyPath The path on the file system to the public key to be
	 *        used for encrypting any ciphers passed to the Encryption-Server
	 *        for persisting.
	 * @param aEncryptionServer The server to which the service is being
	 *        "connected"
	 * @param aCipherVersionGenerator The generator to be used for generating
	 *        {@link CipherVersion} instances.
	 * @param aCipherVersionFactory The factory to be used for creating
	 *        {@link CipherVersion} instances.
	 * 
	 * @throws CryptException in case a crypt exception occurred when
	 *         instantiating the public key
	 * @throws IOException in case reading the file public key caused IO
	 *         problems
	 * @throws GeneralSecurityException in case a security poroblem was
	 *         encountered.
	 */
	public PublicKeyEncryptionService( String aNamespace, String aPublicKeyPath, EncryptionServer aEncryptionServer, CipherVersionGenerator<CipherVersion> aCipherVersionGenerator, CipherVersionFactory<CipherVersion> aCipherVersionFactory ) throws GeneralSecurityException, CryptException, IOException {
		super( aNamespace, aEncryptionServer, aCipherVersionGenerator );
		_cipherVersionFactory = aCipherVersionFactory;
		_publicKeyPath = aPublicKeyPath;
		final File thePublicKeyFile = new File( aPublicKeyPath );
		LOGGER.log( Level.FINE, "Loading public key from file \"" + thePublicKeyFile.getAbsolutePath() + "\"..." );
		try {
			// PublicKey thePublicKey = CryptReader.readPublicKey( thePublicKeyFile );
			final PublicKey thePublicKey = ForwardSecrecyUtility.readPublicKey( thePublicKeyFile );
			_encryptAlgorithm.setKey( thePublicKey );
			_encryptAlgorithm.initEncrypt();
		}
		catch ( FileNotFoundException e ) {
			LOGGER.log( Level.WARNING, "Public key file \"" + thePublicKeyFile.getAbsolutePath() + "\" not found as of: " + Trap.asMessage( e ), e );
			throw e;
		}
		catch ( IOException e ) {
			LOGGER.log( Level.WARNING, "Unable to load public key from file \"" + thePublicKeyFile.getAbsolutePath() + "\" as of : " + Trap.asMessage( e ), e );
			throw e;
		}
		catch ( GeneralSecurityException e ) {
			LOGGER.log( Level.WARNING, "Encountered a security problem loading public key from file \"" + thePublicKeyFile.getAbsolutePath() + "\" as of : " + Trap.asMessage( e ), e );
			throw e;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To encrypted cipher version.
	 *
	 * @param <CV> the generic type
	 * @param aDecryptedCipherVersion the decrypted cipher version
	 * 
	 * @return the cv
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <CV extends CipherVersion> CV toEncryptedCipherVersion( CV aDecryptedCipherVersion ) {
		LOGGER.log( Level.FINE, "Using public key \"" + _publicKeyPath + "\" for encrypting ..." );
		try {
			return (CV) _cipherVersionFactory.create( aDecryptedCipherVersion.getUniversalId(), _encryptAlgorithm.encrypt( aDecryptedCipherVersion.getCipher().getBytes(), _base64Converter ) );
		}
		catch ( CryptException e ) {
			throw new EncryptCipherRuntimeException( "Cannot create encrypted ciphers version from decrypted cipher version!", e );
		}
	}
}
