// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Encoding;
import org.refcodes.runtime.SystemProperty;

@SuppressWarnings("deprecation")
public class ForwardSecrecyTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAMESPACE = "test";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@Disabled
	public void testSample() throws Exception {
		final InMemoryDecryptionServer theDecryptionServer = new InMemoryDecryptionServer();
		final DecryptionService theDecryptionService = new PublicKeyDecryptionService( "myNameSpace", "/path/to/private/key", theDecryptionServer, 500 );
		final DecryptionProvider theDecryptionProvider = new DecryptionProviderImpl( theDecryptionService );
		final InMemoryEncryptionServer theEncryptionServer = new InMemoryEncryptionServer( theDecryptionServer );
		final EncryptionService theEncryptionService = new PublicKeyEncryptionService( "myNameSpace", "/path/to/public/key", theEncryptionServer );
		final EncryptionProvider theEncryptionProvider = new EncryptionProviderImpl( theEncryptionService );
		final String theText = "Hallo Welt!";
		final String theEncryptedText = theEncryptionProvider.toEncrypted( theText );
		String eEncryptedText;
		String eDecryptedText;
		for ( int i = 0; i < 50; i++ ) {
			theEncryptionProvider.nextCipherVersion();
			eEncryptedText = theEncryptionProvider.toEncrypted( theText );
			eDecryptedText = theDecryptionProvider.toDecrypted( theEncryptedText );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eEncryptedText + " -> " + eDecryptedText );
			}
			assertEquals( theText, eDecryptedText );
		}
	}

	@Test
	public void testCryptographyInfrystructure() throws Exception {
		final InMemoryDecryptionServer theDecryptionServer = new InMemoryDecryptionServer();
		final LoopbackDecryptionService theDecryptionService = new LoopbackDecryptionService( NAMESPACE, theDecryptionServer );
		theDecryptionService.setCipherVersionsExpireTimeMillis( 500 );
		final DecryptionProvider theDecryptionProvider = new DecryptionProviderImpl( theDecryptionService );
		final EncryptionServer theEncryptionServer = new InMemoryEncryptionServer( theDecryptionServer );
		final EncryptionService theEncryptionService = new LoopbackEncryptionService( NAMESPACE, theEncryptionServer );
		final EncryptionProvider theEncryptionProvider = new EncryptionProviderImpl( theEncryptionService );
		// @formatter:off
		// CryptographyProvider theCryptographyProvider = new CryptographyProviderImpl( theEncryptionProvider, theDecryptionProvider );
		// @formatter:on
		final String theText = "Hallo Welt!";
		final String theEncryptedText = theEncryptionProvider.toEncrypted( theText );
		String theDecryptedText = theDecryptionProvider.toDecrypted( theEncryptedText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEncryptedText + " -> " + theDecryptedText );
		}
		assertEquals( theText, theDecryptedText );
		theEncryptionProvider.nextCipherVersion();
		final String theEncryptedText2 = theEncryptionProvider.toEncrypted( theText );
		final String theDecryptedText2 = theDecryptionProvider.toDecrypted( theEncryptedText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEncryptedText2 + " -> " + theDecryptedText2 );
		}
		assertEquals( theText, theDecryptedText2 );
		theDecryptedText = theDecryptionProvider.toDecrypted( theEncryptedText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEncryptedText + " -> " + theDecryptedText );
		}
		assertEquals( theText, theDecryptedText );
		String eEncryptedText;
		String eDecryptedText;
		for ( int i = 0; i < 50; i++ ) {
			theEncryptionProvider.nextCipherVersion();
			eEncryptedText = theEncryptionProvider.toEncrypted( theText );
			eDecryptedText = theDecryptionProvider.toDecrypted( theEncryptedText );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eEncryptedText + " -> " + eDecryptedText );
			}
			assertEquals( theText, eDecryptedText );
		}
	}

	@Test
	public void testCryptographyProvider() throws Exception {
		final InMemoryDecryptionServer theDecryptionServer = new InMemoryDecryptionServer();
		final LoopbackDecryptionService theDecryptionService = new LoopbackDecryptionService( NAMESPACE, theDecryptionServer );
		theDecryptionService.setCipherVersionsExpireTimeMillis( 500 );
		final DecryptionProvider theDecryptionProvider = new DecryptionProviderImpl( theDecryptionService );
		final EncryptionServer theEncryptionServer = new InMemoryEncryptionServer( theDecryptionServer );
		final EncryptionService theEncryptionService = new LoopbackEncryptionService( NAMESPACE, theEncryptionServer );
		final EncryptionProvider theEncryptionProvider = new EncryptionProviderImpl( theEncryptionService );
		// @formatter:off
		// CryptographyProvider theCryptographyProvider = new CryptographyProviderImpl( theEncryptionProvider, theDecryptionProvider );
		// @formatter:on
		final String theInput = "Hallo Welt!";
		final String theEncryptedText1 = theEncryptionProvider.toEncrypted( theInput );
		final String theOutputText1 = theDecryptionProvider.toDecrypted( theEncryptedText1 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEncryptedText1 + " -> " + theOutputText1 );
		}
		assertEquals( theInput, theOutputText1 );
		final byte[] theInputHex2 = theInput.getBytes( Encoding.UTF_8.getCode() );
		final byte[] theEncryptedHex2 = new byte[theEncryptedText1.getBytes( Encoding.UTF_8.getCode() ).length];
		theEncryptionProvider.toEncrypted( theInputHex2, 0, theInputHex2.length, theEncryptedHex2, 0 );
		final String theEncryptedText2 = new String( theEncryptedHex2, Encoding.UTF_8.getCode() );
		final byte[] theDecryptedHex2 = new byte[theInputHex2.length];
		theDecryptionProvider.toDecrypted( theEncryptedHex2, 0, theEncryptedHex2.length, theDecryptedHex2, 0 );
		final String theOutputText2 = new String( theDecryptedHex2, Encoding.UTF_8.getCode() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEncryptedText2 + " -> " + theOutputText2 );
		}
		assertEquals( theInput, theOutputText2 );
	}
}
