// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;
import org.refcodes.runtime.SystemProperty;

public class ForwardSecrecyUtilityTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testForwardSecrecyUtility() {
		String eCipherUid;
		String eCipher;
		String eEncryptedMockText;
		String eMockText;
		for ( int i = 0; i < 50; i++ ) {
			eCipherUid = ForwardSecrecyUtility.createCipherUid();
			eCipher = ForwardSecrecyUtility.createCipher();
			assertEquals( eCipherUid.length(), ForwardSecrecyUtility.CIPHER_UID_LENGTH );
			assertEquals( eCipher.length(), ForwardSecrecyUtility.CIPHER_LENGTH );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCipherUid + " --> " + eCipher );
			}
			eEncryptedMockText = eCipherUid + Delimiter.CIPHER_UID.getChar() + i;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eEncryptedMockText + " = " + ( ForwardSecrecyUtility.hasEncryptionPattern( eEncryptedMockText ) ? "Fits encryped pattern" : "Plain" ) );
			}
			assertTrue( ForwardSecrecyUtility.hasEncryptionPattern( eEncryptedMockText ) );
			eMockText = eCipherUid + i;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eMockText + " = " + ( ForwardSecrecyUtility.hasEncryptionPattern( eMockText ) ? "Fits encryped pattern" : "Plain" ) );
			}
			assertFalse( ForwardSecrecyUtility.hasEncryptionPattern( eMockText ) );
			eMockText = i + eCipherUid + Delimiter.CIPHER_UID.getChar();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eMockText + " = " + ( ForwardSecrecyUtility.hasEncryptionPattern( eMockText ) ? "Fits encryped pattern" : "Plain" ) );
			}
			assertFalse( ForwardSecrecyUtility.hasEncryptionPattern( eMockText ) );
		}
	}
}
