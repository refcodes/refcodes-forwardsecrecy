// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.cryptacular.generator.KeyPairGenerator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class ForwardSecrecyWorkshopTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int RSA_BIT_LENGTH = 2048;
	private static final SecureRandom SECURE_RANDOM;
	static {
		out: {
			SecureRandom theSecureRandom = null;
			try {
				theSecureRandom = SecureRandom.getInstanceStrong();
			}
			catch ( NoSuchAlgorithmException e ) {
				SECURE_RANDOM = new SecureRandom();
				break out;
			}
			SECURE_RANDOM = theSecureRandom;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String PATH_TO_SERVICE_KEY = "/tmp/rsa-service-key.pem";
	private static final String PATH_TO_SERVICE_PUB_KEY = "/tmp/rsa-service-pub-key.pem";
	private static final String PATH_TO_SERVER_KEY = "/tmp/rsa-server-key.pem";
	private static final String PATH_TO_SERVER_PUB_KEY = "/tmp/rsa-server-pub-key.pem";
	private static final String HALLO_WELT = "Hallo Welt";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("As of being a workshop no units are tested. Caution: When enabling, files are written to the FS!")
	@Test
	public void testAsysmetricCryptography() throws Exception {
		// WARMUP:
		// RSA theRsa = new RSA();
		// KeyPair theServerKeyPair = theRsa.generateKeys( 2048 );
		// KeyPair theServiceKeyPair = theRsa.generateKeys( 2048 );
		final KeyPair theServerKeyPair = KeyPairGenerator.generateRSA( SECURE_RANDOM, RSA_BIT_LENGTH );
		writeKeyToFile( "The server's private key", PATH_TO_SERVER_KEY, theServerKeyPair.getPrivate().getEncoded() );
		writeKeyToFile( "The server's public key", PATH_TO_SERVER_PUB_KEY, theServerKeyPair.getPublic().getEncoded() );
		final KeyPair theServiceKeyPair = KeyPairGenerator.generateRSA( SECURE_RANDOM, RSA_BIT_LENGTH );
		writeKeyToFile( "The service's private key", PATH_TO_SERVICE_KEY, theServiceKeyPair.getPrivate().getEncoded() );
		writeKeyToFile( "The service's public key", PATH_TO_SERVICE_PUB_KEY, theServiceKeyPair.getPublic().getEncoded() );
		final Cipher theEncryptAlgorithm = Cipher.getInstance( "RSA" );
		theEncryptAlgorithm.init( Cipher.ENCRYPT_MODE, theServerKeyPair.getPublic() );
		byte[] theEncrypted = theEncryptAlgorithm.doFinal( HALLO_WELT.getBytes() );
		final String theEncryptedText = new String( theEncrypted );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypted  = " + theEncryptedText );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Length     = " + theEncryptedText.length() );
		}
		final Cipher theDecryptAlgorithm = Cipher.getInstance( "RSA" );
		theDecryptAlgorithm.init( Cipher.DECRYPT_MODE, theServerKeyPair.getPrivate() );
		byte[] theDecrypted = theDecryptAlgorithm.doFinal( theEncryptedText.getBytes() );
		final String theDecryptedText = new String( theDecrypted );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Decrypted = " + theDecryptedText );
		}
		// TESTSTRING:
		final String theTestString = "0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789";
		// ENCRYPTION:
		long theStartTime = System.currentTimeMillis();
		final int theRuns = 1000;
		for ( int i = 0; i < theRuns; i++ ) {
			theEncrypted = theEncryptAlgorithm.doFinal( ( theTestString + i ).getBytes() );
		}
		long theEndTime = System.currentTimeMillis();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ASYMETRIC ENCRYPTION:" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theRuns + " / seconds = " + ( ( theEndTime - theStartTime ) / 1000 ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "1 / milliseconds = " + ( ( (double) theEndTime - (double) theStartTime ) / theRuns ) );
		}
		// DECRYPTION:
		theStartTime = System.currentTimeMillis();
		for ( int i = 0; i < theRuns; i++ ) {
			theDecrypted = theDecryptAlgorithm.doFinal( theEncrypted );
		}
		theEndTime = System.currentTimeMillis();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ASYMETRIC DECRYPTION:" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theRuns + " / seconds = " + ( ( theEndTime - theStartTime ) / 1000 ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "1 / milliseconds = " + ( ( (double) theEndTime - (double) theStartTime ) / theRuns ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected void writeKeyToFile( String aDescription, String aFileName, byte[] aEncodedKey ) throws IOException {
		try ( PemWriter theServerPemWriter = new PemWriter( new FileWriter( new File( aFileName ) ) ) ) {
			final PemObject theServerPrivateObject = new PemObject( aDescription, aEncodedKey );
			theServerPemWriter.writeObject( theServerPrivateObject );
		}
	}
}
